(function() {
    'use strict';

    console.log("[ Inicializando módulo do AngularJs... ]");

    angular.module("avaliacao", ["ui.bootstrap", "chart.js", "ui.calendar"])
    
    console.log("[ Módulo do AngularJs Inicializado. ]");
    
})();