(function() {
    'use strict';
    console.log("[ inicioService -  Service ronda iniciado ]");
    angular.module("avaliacao").factory('inicioService', inicioService);
    inicioService.$inject = ['$http'];

    function inicioService($http) {
        var vm = this;

        //Variáveis
        vm.error = "";

        return {
        	getAllRondaForDate: getAllRondaForDate,
            saveNotification: saveNotification,
            getAllValidNotification: getAllValidNotification,
            updateNotification: updateNotification,
            deleteNotification: deleteNotification,
            getError: getError,
            setError: setError
        };

        function getError() {
            return vm.error;
        }

        function setError() {
            vm.error = "";
        }
        
        function getAllRondaForDate(data_){
         	console.log("Data:");
        	console.log(data_);
        	return $http({
                url: '/avaliacao/public/ronda/search',
                data: 'data_=' + data_,
                method: "POST",
                headers: {
                	'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
            .success(function(data, status, headers, config) {
                return data;
            })
            .error(function(data, status, headers, config) {
                vm.error = data;
            });
        }

        function saveNotification(avaliacaoNotificationVO) {
            return $http({
                    url: '/avaliacao/protected/notification/create',
                    data: avaliacaoNotificationVO,
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .success(function(data, status, headers, config) {
                    return data;
                })
                .error(function(data, status, headers, config) {
                    vm.error = data;
                });
        }

        function getAllValidNotification() {
            return $http({
                    url: '/avaliacao/protected/notification/read',
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .success(function(data, status, headers, config) {
                    return data;
                })
                .error(function(data, status, headers, config) {
                    vm.error = data;
                });
        }

        function updateNotification(avaliacaoNotificationVO) {
            return $http({
                    url: '/avaliacao/protected/notification/update',
                    data: avaliacaoNotificationVO,
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .success(function(data, status, headers, config) {
                    return data;
                })
                .error(function(data, status, headers, config) {
                    vm.error = data;
                });
        }

        function deleteNotification(idNotification) {
            return $http({
                    url: '/avaliacao/protected/notification/delete',
                    data: 'idNotification=' + idNotification,
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                })
                .success(function(data, status, headers, config) {
                    alert("Notificação deletada com sucesso!");
                })
                .error(function(data, status, headers, config) {
                    alert("Notificação não pode ser deletada com sucesso!");
                });
        }
    }
})();
