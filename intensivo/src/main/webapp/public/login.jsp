<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="col-md-4 col-sm-3 col-xs-1"></div>
<div class="col-md-4 col-sm-6 col-xs-10">

	<form id="fLogin" name="fLogin" ng-controller="utilsController as vm" action="j_spring_security_check" method="post">
		<div class="text-center">
			<img alt="logo" src="public/resources/images/logo.png">
			<!-- <h1 id="login">Painel de acesso</h1>
			<p>Faça login com seu email e senha para ter acesso ao painel.</p> -->
			<hr />
		</div>
		<div class="form-group text-center">
			<div class="col-xs-12 input-group">
				<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
				<input id="j_username" name="j_username" type="email" ng-model="vm.j_username" class="form-control" placeholder="Email" required>
			</div>
			<div ng-show="fLogin.$submitted || fLogin.j_username.$touched">
				<div ng-show="fLogin.j_username.$error.required">
					<p class="wrong">O email é obrigatório para o login.</p>
				</div>
				<div ng-show="fLogin.j_username.$error.email">
					<p class="wrong">O campo email deverá conter um email.</p>
				</div>
			</div>
		</div>

		<div class="form-group text-center">
			<div class="col-xs-12 input-group">
				<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
				<input id="j_password" name="j_password" type="password" ng-model="vm.j_password" ng-bind="vm.capsLockisOn('#fLogin')" class="form-control" placeholder="Senha" required>
			</div>
			<div ng-show="fLogin.$submitted || fLogin.j_password.$touched">
				<div ng-show="fLogin.j_password.$error.required">
					<p class="wrong">A senha é obrigatória para o login.</p>
				</div>
			</div>
		</div>

		<div ng-show="'${errorMessage}' != ''">
			<div class="text-center alert alert-danger alert-dismissable fade in">
				${errorMessage}</div>
		</div>

		<div ng-show="vm.control.bol">
			<div
				class="text-center alert alert-warning alert-dismissable fade in">
				{{vm.control.error}}</div>
		</div>

		<div class="form-group">
			<div class="col-xs-12 input-group text-center">
				<button type="submit" name="submit" class="btn btn-primary">
					<span class="glyphicon glyphicon-log-in"></span>&nbsp; Entrar
				</button>
			</div>
		</div>
	</form>

</div>
<div class="col-md-4 col-sm-3 col-xs-1"></div>

<script src="public/controller/loginController.js"></script>
<script src="resources/js/utilsController.js"></script>
