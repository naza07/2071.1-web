<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!doctype html>
<html lang="pt-BR" ng-app="avaliacao">
<head>
<title>Intensivo</title>
<meta charset='utf-8'>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="shortcut icon" type="image/x-icon" href="resources/images/favicon.ico">

<!-- Bootstrap -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">
<link href="resources/css/style.css" rel="stylesheet">

<!-- jQuery (obrigatório para plugins JavaScript do Bootstrap) -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<!-- Inclui todos os plugins compilados (abaixo), ou inclua arquivos separadados se necessário -->
<script src="resources/js/bootstrap.min.js"></script>

<!-- AngularJS -->
<script src="resources/js/angular/angular.min.js"></script>
<script src="resources/js/angular/angular-animate.min.js"></script>
<script src="resources/js/angular/angular-touch.min.js"></script>
<script src="resources/js/ui-bootstrap-2.3.0.min.js"></script>
<script src="angularInit.js"></script>


<!-- Graficos -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.6/Chart.bundle.min.js"></script>
<script src="resources/js/angular-chart/angular-chart.min.js"></script>

<!-- Calendario -->
<script src="resources/js/calendar/moment-with-locales.js"></script>
<script src="resources/js/calendar/calendar.js"></script>
<script src="resources/js/calendar/fullcalendar.min.js"></script>
<script src="resources/js/calendar/gcal.js"></script>
<script src="resources/js/calendar/locale-all.js"></script>
<link href="resources/css/fullcalendar.css" rel="stylesheet">

</head>

<body ng-cloak>
	<div class="container">
		<div class="row">
			<tiles:insertAttribute name="header" />
		</div>
	</div>
	<div class="container">
		<div class="row">
			<tiles:insertAttribute name="body" />
		</div>
	</div>
	<div class="container">
		<div class="row">
			<tiles:insertAttribute name="footer" />
		</div>
	</div>
</body>
</html>
