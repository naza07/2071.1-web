<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="col-xs-12 fixed_footer">
	<nav class="navbar navbar-default navbar-fixed-bottom">
		<div class="container-fluid">
			<div class="row-fluid">
				<p class="navbar-text pull-right">
					Copyright <i class="glyphicon glyphicon-copyright-mark"></i> 2017 <a href="home">Intensivo</a>. Todos os direitos reservados.
				</p>
			</div>
		</div>
	</nav>
</div>
