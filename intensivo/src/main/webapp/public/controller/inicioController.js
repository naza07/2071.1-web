(function() {
	'use strict';
	console.log("[ inicioController -  Controlador ronda iniciado ]");

	angular.module("avaliacao")
			.controller('inicioController', inicioController);

	inicioController.$inject = [ 'inicioService' ];

	function inicioController(inicioService) {
		var vm = this;

		// variáveis
		vm.ronda = null;
		vm.rondaList = [];
		vm.date_f = "";

		// funções
		vm.getAllRondaForDate = getAllRondaForDate;
		vm.initRonda = initRonda;

		vm.getAllValidNotification = getAllValidNotification;
		vm.saveNotification = saveNotification;
		vm.deleteNotification = deleteNotification;
		vm.updateNotification = updateNotification;
		vm.exibirDados = exibirDados;
		vm.showError = showError;
		vm.getDateFormated = getDateFormated;

		function initRonda() {
			vm.ronda = {
				idavaliacaoronda : 0,
				cidade : "",
				loja : "",
				visitado : "",
			};
			return vm.ronda;
		}

		function getAllRondaForDate(data_) {
			console.log("DATA: " + data_);
			console.log("ok");
			vm.getDateFormated(data_);
			/*inicioService.getAllRondaForDate(data_).then(
					function(data, status, headers, config) {
						vm.rondaList = data.data;
					});*/
		}

		function getAllValidNotification() {

			notificationService.getAllValidNotification().then(
					function(data, status, headers, config) {
						vm.notificationList = data.data;
					});

		}

		function saveNotification(notification, fRegister) {
			notificationService.saveNotification(notification).then(
					function(data, status, headers, config) {
						vm.notificationList.push(notification);
						if (!vm.continuar) {
							$('#RegisterModal').modal('hide');
						}
					});

			resetForm(fRegister);
		}

		function deleteNotification(idNotification, notification) {
			notificationService.deleteNotification(idNotification).then(
					function(data, status, headers, config) {
						var index = vm.notificationList.indexOf(notification);
						vm.notificationList.splice(index, 1);
					});
		}

		function updateNotification(avaliacaoNotificationVO, fRegister) {

			notificationService.updateNotification(avaliacaoNotificationVO)
					.then(function(data, status, headers, config) {
						$('#RegisterModal').modal('hide');
						vm.resetForm(fRegister);
					});
		}

		function exibirDados(notification) {
			vm.notification = notification;
		}

		function showError() {
			return notificationService.getError();
		}

		function getRonda(ronda) {

			vm.rondaList = [];
			ronda.idavaliacaoronda = 1;
			ronda.cidade = "Recife";
			ronda.loja = "Fiori";
			ronda.visitado = "Não";
			ronda.data_ = "17/06/2017";
			vm.rondaList.push(ronda);
			return vm.rondaList;
		}
		
		function getDateFormated(d){
			var date = d;
			var month = date.getMonth();
			month = month + 1;
			if(month<10){
				month = "0"+month;
			}
			var day = date.getDate();
			var year = date.getFullYear();
			var formated = day+"/"+month+"/"+year
			return vm.date_f = formated;
		};
		

		// inicializações var n =
		var n = vm.initRonda();
		// console.log(n);
		//getRonda(n);
		// vm.getAllValidNotification();

	}
})();
