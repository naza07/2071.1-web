(function() {
    'use strict';
    console.log("[ utilsController -  Controlador iniciado ]");

    angular.module("avaliacao").controller('utilsController', utilsController);

    utilsController.$inject = [];

    function utilsController() {
        var vm = this;

        //variáveis
        vm.control = {error: "", bol : false};

        //funções
        vm.capsLockisOn = capsLockisOn;

        function capsLockisOn(id){
          $(id).keypress(function(e) {
              var s = String.fromCharCode( e.which );
              if ( s.toUpperCase() === s && s.toLowerCase() !== s && !e.shiftKey ) {
                  vm.control.error = "O Caps Lock está ativo!";
                  vm.control.bol = true;
              }
              else{
              vm.control.error = "";
              vm.control.bol = false;
              }
          });
        }

        }
})();
