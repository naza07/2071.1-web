<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div ng-controller="homeController as vm">
<div class="col-md-4 col-sm-12 spacing">
	<div class="bg_box">
		<div class="row text-center">
			<div class="col-md-4 col-xs-4 "><img src="../protected/resources/images/graduate.png" alt="usuarios"></div>
			<div class="col-md-8 col-xs-8"><h3>Resumo dos Usuários</h3></div>
		</div>
		<div class="row" style="margin-top: 20px;">
			
			<canvas id="doughnut" class="chart chart-doughnut" chart-data="vm.data" chart-labels="vm.labels" chart-colors="vm.colors" ng-if="!(vm.prof == 0 && vm.alun == 0 && vm.resp == 0)"></canvas>
			
			<div class="row" style="padding-top: 40px; padding-left: 15px; padding-right: 15px;">
				<div class="col-sm-12">
					<p>O numero de <strong>alunos</strong> <strong>validos</strong> no sistema é: <strong>{{vm.alun.length}}</strong></p>
					<!-- <p>O numero <strong>total</strong> de <strong>alunos</strong> cadastrados no sistema é: {{}}</p>  -->
					<br>
				</div>
				<div class="col-sm-12">
					<p>O numero de <strong>professores</strong> <strong>validos</strong> no sistema é: <strong>{{vm.prof.length}}</strong></p>
					<!-- <p>O numero <strong>total</strong> de <strong>professores</strong> cadastrados no sistema é: {{}}</p> -->
					<br>
				</div>
				<div class="col-sm-12">
					<p>O numero de <strong>responsaveis</strong> <strong>validos</strong> no sistema é: <strong>{{vm.resp.length}}</strong></p>
					<!-- <p>O numero <strong>total</strong> de <strong>responsaveis</strong> cadastrados no sistema é: {{}}</p> -->
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-md-8 col-sm-12">
	<div class="row ">
		<div class="col-md-6 col-sm-12 spacing">
			<div class="bg_box_half">
				<div class="row text-center">
					<div class="col-md-4 col-xs-4"><img src="../protected/resources/images/notebook.png" alt="materiais"></div>
					<div class="col-md-8 col-xs-8"><h3>Resumo dos Materiais</h3></div>
				</div>
				<div class="row" style="margin-top: 20px;">
					<canvas id="bar" class="chart chart-bar" chart-data="vm.data_mat" chart-labels="vm.labels_mat" chart-series="vm.series_mat"></canvas>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-sm-12 spacing">
			<div class="bg_box_half">
				<div class="row text-center">
					<div class="col-md-3 col-xs-4"><img src="../protected/resources/images/warning.png" alt="notificações"></div>
					<div class="col-md-9 col-xs-8"><h3>Resumo das Notificações</h3></div>
				</div>
				<div class="row" style="margin-top: 20px;">
					<canvas id="bar2" class="chart chart-bar"  chart-data="vm.data_not" chart-labels="vm.labels_not" chart-series="vm.series_not"> </canvas>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 col-sm-12">
			<div class="bg_box_half spacing">
				<div class="row text-center">
					<div class="col-md-4 col-xs-4"><img src="../protected/resources/images/calendar.png" alt="horarios"></div>
					<div class="col-md-8 col-xs-8"><h3>Resumo dos Horários</h3></div>
				</div>
				<div class="row" style="margin-top: 20px;">
					<canvas id="bar3" class="chart chart-bar"  chart-data="vm.data_hor" chart-labels="vm.labels_hor" chart-series="vm.series_hor"> </canvas>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-sm-12">
			<div class="bg_box_half">
				<div class="row text-center">
					<div class="col-md-3 col-xs-4"><img src="../protected/resources/images/text-lines.png" alt="propagandas"></div>
					<div class="col-md-9 col-xs-8"><h3>Resumo das Propagandas</h3></div>
				</div>
				<div class="row" style="margin-top: 20px;">
					<canvas id="bar4" class="chart chart-bar"  chart-data="vm.data_prop" chart-labels="vm.labels_prop" chart-series="vm.series_prop"> </canvas>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<script src="../protected/controller/homeController.js"></script>
<script src="../protected/service/usuariosService.js"></script>
<script src="../protected/service/materialService.js"></script>
<script src="../protected/service/notificacoesService.js"></script>
<script src="../protected/service/propagandaService.js"></script>
