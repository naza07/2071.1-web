(function() {
    'use strict';
    console.log("[ cadastroResponsavelService -  Service iniciado ]");
    angular.module("avaliacao").factory('cadastroResponsavelService', cadastroResponsavelService);
    cadastroResponsavelService.$inject = ['$http'];

    function cadastroResponsavelService($http) {
        var vm = this;

        //Variáveis
        vm.error = "";

        return {
            saveUser: saveUser,
            getAllValidUsers: getAllValidUsers,
            updateUser: updateUser,
            deleteUser: deleteUser,
            getError: getError,
            setError: setError
        };

        function getError() {
            return vm.error;
        }

        function setError() {
            vm.error = "";
        }

        function saveUser(responsavelUsuarioVO) {
            return $http({
                    url: '/intensivo/protected/usuarios/insertResponsavel',
                    data: responsavelUsuarioVO,
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .success(function(data, status, headers, config) {
                    return data;
                })
                .error(function(data, status, headers, config) {
                    vm.error = data;
                });
        }

        function getAllValidUsers() {
            return $http({
                    url: '/avaliacao/protected/user/read',
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .success(function(data, status, headers, config) {
                    return data;
                })
                .error(function(data, status, headers, config) {
                    vm.error = data;
                });
        }

        function updateUser(avaliacaoUsuarioVO) {
            return $http({
                    url: '/avaliacao/protected/user/update',
                    data: avaliacaoUsuarioVO,
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .success(function(data, status, headers, config) {
                    return data;
                })
                .error(function(data, status, headers, config) {
                    vm.error = data;
                });
        }

        function deleteUser(idUsuario) {
            return $http({
                    url: '/avaliacao/protected/user/delete',
                    data: 'idUsuario=' + idUsuario,
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                })
                .success(function(data, status, headers, config) {
                    alert("Usuario deletado com sucesso!");
                })
                .error(function(data, status, headers, config) {
                    alert("Usuario não pode ser deletado com sucesso!");
                });
        }
    }
})();