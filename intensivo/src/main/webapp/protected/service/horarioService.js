(function() {
    'use strict';
    console.log("[ horarioService -  Service iniciado ]");
    angular.module("avaliacao").factory('horarioService', horarioService);
    horarioService.$inject = ['$http'];

    function horarioService($http) {
        var vm = this;

        //Variáveis
        vm.error = "";

        return {
        	saveHorario: saveHorario,
            getAllValidHorario: getAllValidHorario,
            updateHorario: updateHorario,
            deleteHorario: deleteHorario,
            getError: getError,
            setError: setError
        };

        function getError() {
            return vm.error;
        }

        function setError() {
            vm.error = "";
        }

        function saveHorario(horarioVO) {
            return $http({
                    url: '/intensivo/protected/horarios/insert',
                    data: horarioVO,
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .success(function(data, status, headers, config) {
                    return data;
                })
                .error(function(data, status, headers, config) {
                    vm.error = data;
                });
        }

        function getAllValidHorario() {
            return $http({
                    url: '/intensivo/protected/horarios/getAllHorario',
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .success(function(data, status, headers, config) {
                    return data;
                })
                .error(function(data, status, headers, config) {
                    vm.error = data;
                });
        }

        function updateHorario(horarioVO) {
            return $http({
                    url: '/intensivo/protected/horarios/update',
                    data: horarioVO,
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .success(function(data, status, headers, config) {
                    return data;
                })
                .error(function(data, status, headers, config) {
                    vm.error = data;
                });
        }

        function deleteHorario(idHorario) {
            return $http({
                    url: '/intensivo/protected/horarios/delete',
                    data: 'idHorario=' + idHorario,
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                })
                .success(function(data, status, headers, config) {
                    alert("Horario deletado com sucesso!");
                })
                .error(function(data, status, headers, config) {
                    alert("Horario não pode ser deletado com sucesso!");
                });
        }
    }
})();