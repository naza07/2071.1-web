(function() {
    'use strict';
    console.log("[ propagandaService -  Service iniciado ]");
    angular.module("avaliacao").factory('propagandaService', propagandaService);
    propagandaService.$inject = ['$http'];

    function propagandaService($http) {
        var vm = this;

        //Variáveis
        vm.error = "";
        
        return {
        	savePropaganda: savePropaganda,
        	getAllValidPropaganda: getAllValidPropaganda,
            updatePropaganda: updatePropaganda,
            deletePropaganda: deletePropaganda,
            getError: getError,
            setError: setError
        };

        function getError() {
            return vm.error;
        }

        function setError() {
            vm.error = "";
        }
        
        function savePropaganda(propagandaVO) {
            return $http({
                    url: '/intensivo/protected/propagandas/insertPropaganda',
                    data: propagandaVO,
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .success(function(data, status, headers, config) {
                	alert("Propaganda cadastrada com sucesso!");
                    return data;
                })
                .error(function(data, status, headers, config) {
                    vm.error = data;
                });
        }
        
        function getAllValidPropaganda() {
            return $http({
                    url: '/intensivo/protected/propagandas/getPropagandas',
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .success(function(data, status, headers, config) {
                    return data;
                })
                .error(function(data, status, headers, config) {
                    vm.error = data;
                });
        }
        
        function updatePropaganda(propagandaVO) {
            return $http({
                    url: '/intensivo/protected/propagandas/updatePropaganda',
                    data: propagandaVO,
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .success(function(data, status, headers, config) {
                    return data;
                })
                .error(function(data, status, headers, config) {
                    vm.error = data;
                });
        }
        
        function deletePropaganda(idPropaganda) {
            return $http({
                    url: '/intensivo/protected/propagandas/delete',
                    data: 'idPropaganda=' + idPropaganda,
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                })
                .success(function(data, status, headers, config) {
                    alert("Propaganda deletada com sucesso!");
                })
                .error(function(data, status, headers, config) {
                    alert("Propaganda não pòde ser deletada com sucesso!");
                });
        }
    }
     
})();