(function() {
    'use strict';
    console.log("[ usuariosService -  Service iniciado ]");
    angular.module("avaliacao").factory('usuariosService', usuariosService);
    usuariosService.$inject = ['$http'];

    function usuariosService($http) {
        var vm = this;

        //Variáveis
        vm.error = "";

        return {
            getAllValidUsers: getAllValidUsers,
            deleteUser: deleteUser,
            
            saveUserAluno: saveUserAluno,
            updateUserAluno: updateUserAluno,
           
            
            saveUserProfessor: saveUserProfessor,
            updateUserProfessor: updateUserProfessor,
         
            
            saveUserResponsavel: saveUserResponsavel,
            updateUserResponsavel: updateUserResponsavel,
        
            
            getError: getError,
            setError: setError
        };
        
        function getAllValidUsers() {
            return $http({
                    url: '/intensivo/protected/usuarios/getUsuarios',
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .success(function(data, status, headers, config) {
                    return data;
                })
                .error(function(data, status, headers, config) {
                    vm.error = data;
                });
        }

        function getError() {
            return vm.error;
        }

        function setError() {
            vm.error = "";
        }
        
        function deleteUser(idUsuario) {
            return $http({
                    url: '/intensivo/protected/usuarios/delete',
                    data: 'idUsuario=' + idUsuario,
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                })
                .success(function(data, status, headers, config) {
                    alert("Usuario deletado com sucesso!");
                })
                .error(function(data, status, headers, config) {
                    alert("Usuario não pode ser deletado com sucesso!");
                });
        }

        //Usuário Responsável
        function saveUserResponsavel(responsavelUsuarioVO) {
            return $http({
                    url: '/intensivo/protected/usuarios/insertResponsavel',
                    data: responsavelUsuarioVO,
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .success(function(data, status, headers, config) {
                    return data;
                })
                .error(function(data, status, headers, config) {
                    vm.error = data;
                });
        }

       

        function updateUserResponsavel(avaliacaoUsuarioVO) {
            return $http({
                    url: '/intensivo/protected/usuarios/updateResponsavel',
                    data: avaliacaoUsuarioVO,
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .success(function(data, status, headers, config) {
                	 alert("Usuario atualizado com sucesso!");
                    return data;
                })
                .error(function(data, status, headers, config) {
                    vm.error = data;
                });
        }

        
        //Usuário Professor
        function saveUserProfessor(professorUsuarioVO) {
            return $http({
                    url: '/intensivo/protected/usuarios/insertProfessor',
                    data: professorUsuarioVO,
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .success(function(data, status, headers, config) {
                    return data;
                })
                .error(function(data, status, headers, config) {
                    vm.error = data;
                });
        }

       

        function updateUserProfessor(professorUsuarioVO) {
            return $http({
                    url: '/intensivo/protected/usuarios/updateProfessor',
                    data: professorUsuarioVO,
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .success(function(data, status, headers, config) {
                	 alert("Usuario atualizado com sucesso!");
                    return data;
                })
                .error(function(data, status, headers, config) {
                    vm.error = data;
                });
        }

        //Usuário Aluno
        function saveUserAluno(alunoUsuarioVO) {
            return $http({
                    url: '/intensivo/protected/usuarios/insertAluno',
                    data: alunoUsuarioVO,
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .success(function(data, status, headers, config) {
                    return data;
                })
                .error(function(data, status, headers, config) {
                    vm.error = data;
                });
        }

       

        function updateUserAluno(alunoUsuarioVO) {
            return $http({
                    url: '/intensivo/protected/usuarios/updateAluno',
                    data: alunoUsuarioVO,
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .success(function(data, status, headers, config) {
                	 alert("Usuario atualizado com sucesso!");
                    return data;
                })
                .error(function(data, status, headers, config) {
                    vm.error = data;
                });
        }

    }
})();