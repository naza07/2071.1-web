(function() {
    'use strict';
    console.log("[ materialService -  Service iniciado ]");
    angular.module("avaliacao").factory('materialService', materialService);
    materialService.$inject = ['$http'];

    function materialService($http) {
        var vm = this;

        //Variáveis
        vm.error = "";

        return {
        	saveMaterial: saveMaterial,
            getAllValidMaterial: getAllValidMaterial,
            updateMaterial: updateMaterial,
            deleteMaterial: deleteMaterial,
            getError: getError,
            setError: setError
        };

        function getError() {
            return vm.error;
        }

        function setError() {
            vm.error = "";
        }

        function saveMaterial(materialVO) {
            return $http({
                    url: '/intensivo/protected/material/insert',
                    data: materialVO,
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .success(function(data, status, headers, config) {
                    return data;
                })
                .error(function(data, status, headers, config) {
                    vm.error = data;
                });
        }

        function getAllValidMaterial() {
            return $http({
                    url: '/intensivo/protected/material/getAllMaterial',
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .success(function(data, status, headers, config) {
                    return data;
                })
                .error(function(data, status, headers, config) {
                    vm.error = data;
                });
        }

        function updateMaterial(materialVO) {
            return $http({
                    url: '/intensivo/protected/material/update',
                    data: materialVO,
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .success(function(data, status, headers, config) {
                    return data;
                })
                .error(function(data, status, headers, config) {
                    vm.error = data;
                });
        }

        function deleteMaterial(idMaterial) {
            return $http({
                    url: '/intensivo/protected/material/delete',
                    data: 'idMaterial=' + idMaterial,
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                })
                .success(function(data, status, headers, config) {
                    alert("Material deletado com sucesso!");
                })
                .error(function(data, status, headers, config) {
                    alert("Material não pode ser deletado com sucesso!");
                });
        }
    }
})();