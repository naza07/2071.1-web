(function() {
    'use strict';
    console.log("[ cadastroService -  Service iniciado ]");
    angular.module("avaliacao").factory('cadastroService', cadastroService);
    cadastroService.$inject = ['$http'];

    function cadastroService($http) {
        var vm = this;

        //Variáveis
        vm.error = "";

        return {
            saveUser: saveUser,
            getAllValidUsers: getAllValidUsers,
            updateUser: updateUser,
            deleteUser: deleteUser,
            getError: getError,
            setError: setError
        };

        function getError() {
            return vm.error;
        }

        function setError() {
            vm.error = "";
        }

        function saveUser(avaliacaoUsuarioVO) {
            return $http({
                    url: '/intensivo/protected/user/create',
                    data: avaliacaoUsuarioVO,
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .success(function(data, status, headers, config) {
                    return data;
                })
                .error(function(data, status, headers, config) {
                    vm.error = data;
                });
        }

        function getAllValidUsers() {
            return $http({
                    url: '/intensivo/protected/user/read',
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .success(function(data, status, headers, config) {
                    return data;
                })
                .error(function(data, status, headers, config) {
                    vm.error = data;
                });
        }

        function updateUser(avaliacaoUsuarioVO) {
            return $http({
                    url: '/intensivo/protected/user/update',
                    data: avaliacaoUsuarioVO,
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .success(function(data, status, headers, config) {
                    return data;
                })
                .error(function(data, status, headers, config) {
                    vm.error = data;
                });
        }

        function deleteUser(idUsuario) {
            return $http({
                    url: '/intensivo/protected/user/delete',
                    data: 'idUsuario=' + idUsuario,
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                })
                .success(function(data, status, headers, config) {
                    alert("Usuario deletado com sucesso!");
                })
                .error(function(data, status, headers, config) {
                    alert("Usuario não pode ser deletado com sucesso!");
                });
        }
    }
})();