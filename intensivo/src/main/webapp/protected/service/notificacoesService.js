(function() {
	'use strict';
	console.log("[ notificacoesService -  Service iniciado ]");
	angular.module("avaliacao").factory('notificacoesService',
			notificacoesService);
	notificacoesService.$inject = [ '$http' ];

	function notificacoesService($http) {
		var vm = this;

		// Variáveis
		vm.error = "";

		return {
			saveNotificacao : saveNotificacao,
			getAllValidNotificacao : getAllValidNotificacao,
			updateNotificacao : updateNotificacao,
			deleteNotificacao : deleteNotificacao,
			getError : getError,
			setError : setError
		};

		function getError() {
			return vm.error;
		}

		function setError() {
			vm.error = "";
		}

		function saveNotificacao(notificacaoVO) {
			return $http({
				url : '/intensivo/protected/notificacoes/insertNotificacao',
				data : notificacaoVO,
				method : "POST",
				headers : {
					'Content-Type' : 'application/json'
				}
			}).success(function(data, status, headers, config) {
				alert("Notificação salva com sucesso!");
				return data;
			}).error(function(data, status, headers, config) {
				vm.error = data;
			});
		}

		function getAllValidNotificacao() {
			return $http({
				url : '/intensivo/protected/notificacoes/getNotificacoes',
				method : "POST",
				headers : {
					'Content-Type' : 'application/json'
				}
			}).success(function(data, status, headers, config) {
				return data;
			}).error(function(data, status, headers, config) {
				vm.error = data;
			});
		}

		function updateNotificacao(notificacaoVO) {
			return $http({
				url : '/intensivo/protected/notificacoes/updateNotificacao',
				data : notificacaoVO,
				method : "POST",
				headers : {
					'Content-Type' : 'application/json'
				}
			}).success(function(data, status, headers, config) {
				alert("Notificação atualizada com sucesso!");
				return data;
			}).error(function(data, status, headers, config) {
				vm.error = data;
			});
		}

		function deleteNotificacao(idNotificacao) {
			return $http({
				url : '/intensivo/protected/notificacoes/delete',
				data : 'idNotificacao=' + idNotificacao,
				method : "POST",
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				}
			}).success(function(data, status, headers, config) {
				alert("Notificacao deletada com sucesso!");
			}).error(function(data, status, headers, config) {

			});
		}
	}
})();