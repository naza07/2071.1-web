<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar1" aria-expanded="false">
				<span class="sr-only">Navegação</span> <span class="icon-bar"></span>
				<span class="icon-bar"></span> <span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="home"> Intensivo</a>
		</div>
		<div class="collapse navbar-collapse" id="navbar1">
			<ul class="nav navbar-nav navbar-right">
				<li class="active"><a href="home">Bem vindo, ${usuario.nome}!</a></li>
	      		<li ng-class="{'active':vm.selectedTab === 'users'}" ng-click="vm.ativeControl('users')"><a href="usuarios">Usuarios</a></li>
	      		<li ng-class="{'active':vm.selectedTab === 'mat'}" ng-click="vm.ativeControl('mat')"><a href="material">Materiais</a></li>
	      		<li ng-class="{'active':vm.selectedTab === 'notf'}" ng-click="vm.ativeControl('notf')"><a href="notificacoes">Notificações</a></li>
      			<li ng-class="{'active':vm.selectedTab === 'hor'}" ng-click="vm.ativeControl('hor')"><a href="horarios">Horários</a></li>
      			<li ng-class="{'active':vm.selectedTab === 'prop'}" ng-click="vm.ativeControl('prop')"><a href="propagandas">Propagandas</a></li>
				<li><a href="../logout">Sair</a></li>
			</ul>
		</div>
	</div>
</nav>
<script src="../protected/controller/headController.js"></script>
