(function() {
    'use strict';
    console.log("[ horarioController -  Controlador iniciado ]");

    angular.module("avaliacao").controller('horarioController', horarioController);

    horarioController.$inject = ['horarioService', '$scope'];

    function horarioController(horarioService, $scope) {
        var vm = this;
        
        //variaveis
        vm.eventSources = [];
        vm.horario = null;
        vm.horarioList = null;
        vm.continuarCadastro = false;
        
        //funções
        vm.initHorario = initHorario;
        vm.getAllValidHorario = getAllValidHorario;
        vm.saveHorario = saveHorario;
        vm.deleteHorario = deleteHorario;
        vm.updateHorario = updateHorario;
        vm.showError = showError;
        vm.resetForm = resetForm;
        vm.exibirDados = exibirDados;
        
        function initHorario(){
        	vm.horario = {
        			idHorario: 0,
        			dia: "",
        			inicio: null,
        			fim: null,
        			materia: null
        	};
        }
        
        function getAllValidHorario() {
        	horarioService.getAllValidHorario().then(function(data, status, headers, config) {
                vm.horarioList = data.data;
            });
        }
        
        function setTime(date){
        	//var time = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
        	var time = moment(date).format('hh:mm:ss');
        	return time;
        }
        
        function toMilliseconds(time){
        	var a = time.split(':');
        	var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]); 
        	var ms = seconds * 1000;
        	return ms;
        }
        

        function saveHorario(horario, fRegister) {
        	horario.inicio = setTime(horario.inicio);
        	horario.fim = setTime(horario.fim);
        	horarioService.saveHorario(horario).then(function(data, status, headers, config) {
                vm.horarioList.push(horario);
                if (!vm.continuarCadastro) {
                    $('#RegisterModal_horario').modal('hide');
                }
            });
            resetForm(fRegister);
        }
        
        function deleteHorario(idHorario, horario) {
        	horarioService.deleteHorario(idHorario).then(function(data, status, headers, config) {
                var index = vm.horarioList.indexOf(horario);
                vm.horarioList.splice(index, 1);
            });
        }
        
        function updateHorario(horario, fRegister) {
        	horario.inicio = setTime(horario.inicio);
        	horario.fim = setTime(horario.fim);
        	horario.materia = parseInt(horario.materia);
        	horarioService.updateHorario(horario).then(function(data, status, headers, config) {
                $('#RegisterModal_horario').modal('hide');
                vm.resetForm(fRegister);
            });
        }
        
    	function resetForm(fRegister) {
            vm.initHorario();
            fRegister.$setUntouched();
            fRegister.$setPristine();
            horarioService.setError();
        }

        function exibirDados(horario) {
        	
        	horario.inicio = new Date(toMilliseconds(horario.inicio));
        	horario.inicio.setHours(horario.inicio.getHours()+3);
        	
        	horario.fim = new Date(toMilliseconds(horario.fim));
        	horario.fim.setHours(horario.fim.getHours()+3);
        	
        	/*switch(horario.materia) {
            case 1:
                horario.materia = "Portugues";
                break;
            case 2:
            	horario.materia = "Matematica";
                break;
            case 3:
            	horario.materia = "Ciencias";
                break;
            case 4:
            	horario.materia = "Historia";
                break;
            case 5:
            	horario.materia = "Geografia";
                break;
        	}*/

            vm.horario = horario;
        }

        function showError() {
            return horarioService.getError();
        }

        //inicializações
        vm.initHorario();
        vm.getAllValidHorario();
        
        /* config object */
        vm.uiConfig = {
          calendar:{
            height: 450,
            editable: true,
            locale: 'pt-br',
            header:{
              left: 'month basicWeek basicDay agendaWeek agendaDay',
              center: 'title',
              right: 'today prev,next'
            },
            eventClick: $scope.alertEventOnClick,
            eventDrop: $scope.alertOnDrop,
            eventResize: $scope.alertOnResize
          }
        };
        
        
    
    }
})();