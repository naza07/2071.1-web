(function() {
    'use strict';
    console.log("[ cadastroProfessorController -  Controlador iniciado ]");

    angular.module("avaliacao").controller('cadastroProfessorController', cadastroProfessorController);

    cadastroProfessorController.$inject = ['cadastroProfessorService'];

    function cadastroProfessorController(cadastroProfessorService) {
        var vm = this;

        //variáveis
        vm.usuario = null;
        vm.senhaConf = "";
        vm.usuarioList = null;
        vm.continuarCadastro = false;

        //funções
        vm.initUser = initUser;
        vm.resetForm = resetForm;
        vm.confimacaoSenha = confimacaoSenha;
        vm.getAllValidUsers = getAllValidUsers;
        vm.saveUser = saveUser;
        vm.deleteUser = deleteUser;
        vm.updateUser = updateUser;
        vm.exibirDados = exibirDados;
        vm.showError = showError;

        function initUser() {
            vm.usuario = {
                idavaliacaousuario: 0,
                nome: "",
                email: "",
                senha: ""
            };
        }

        function getAllValidUsers() {
        	cadastroProfessorService.getAllValidUsers().then(function(data, status, headers, config) {
                vm.usuarioList = data.data;
            });
        }

        function saveUser(usuario, fRegister) {
        	cadastroProfessorService.saveUser(usuario).then(function(data, status, headers, config) {
                vm.usuarioList.push(usuario);
                if (!vm.continuarCadastro) {
                    $('#RegisterModal_professor').modal('hide');
                }
            });
            resetForm(fRegister);
        }

        function deleteUser(idUsuario, usuario) {
        	cadastroProfessorService.deleteUser(idUsuario).then(function(data, status, headers, config) {
                var index = vm.usuarioList.indexOf(usuario);
                vm.usuarioList.splice(index, 1);
            });
        }

        function updateUser(avaliacaoUsuarioVO, fRegister) {
        	cadastroProfessorService.updateUser(avaliacaoUsuarioVO).then(function(data, status, headers, config) {
                $('#RegisterModal_professor').modal('hide');
                vm.resetForm(fRegister);
            });
        }

        function confimacaoSenha(fRegister) {
            if (vm.usuario.senha != vm.senhaConf) {
                fRegister.senhaConf.$setValidity("senhaConfError", false);
            } else {
                fRegister.senhaConf.$setValidity("senhaConfError", true);
            }
        }

        function resetForm(fRegister) {
            vm.initUser();
            vm.senhaConf = "";
            fRegister.$setUntouched();
            fRegister.$setPristine();
            cadastroProfessorService.setError();
        }

        function exibirDados(usuario) {
            vm.usuario = usuario;
        }

        function showError() {
            return cadastroProfessorService.getError();
        }

        //inicializações
        vm.initUser();
        //vm.getAllValidUsers();

    }
})();