(function() {
    'use strict';
    console.log("[ notificacoesController -  Controlador iniciado ]");

    angular.module("avaliacao").controller('notificacoesController', notificacoesController);

    notificacoesController.$inject = ['notificacoesService'];

    function notificacoesController(notificacoesService) {
        var vm = this;
        
        //variáveis
        vm.notificacao = null;
        vm.notificacaoList = null;
        vm.continuarCadastro = false;
        
        //funções
        vm.initNotificacao = initNotificacao;
        vm.saveNotificacao = saveNotificacao;
        vm.updateNotificacao = updateNotificacao;
        vm.deleteNotificacao = deleteNotificacao;
        vm.getAllValidNotificacao = getAllValidNotificacao;
        vm.resetForm = resetForm;
        vm.exibirDados = exibirDados;
        vm.showError = showError;

        
        function initNotificacao(){
        	vm.notificacao = {
        			idnotificacao: 0,
        			titulo: "",
                	descricao: "",
                	data: ""		
        	};
        }
        
        function getAllValidNotificacao() {  	
        	notificacoesService.getAllValidNotificacao().then(function(data, status, headers, config) {
                vm.notificacaoList = data.data;
            });
        }
        
        function saveNotificacao(notificacao, fRegister) {
        	notificacoesService.saveNotificacao(notificacao).then(function(data, status, headers, config) {
                vm.notificacaoList.push(notificacao);
                if (!vm.continuarCadastro) {
                    $('#RegisterModal_notificacao').modal('hide');
                }
            });
            resetForm(fRegister);
        }
        
        function deleteNotificacao(idNotificacao, notificacao) {
        	notificacoesService.deleteNotificacao(idNotificacao).then(function(data, status, headers, config) {
                var index = vm.notificacaoList.indexOf(notificacao);
                vm.notificacaoList.splice(index, 1);
            });
        }
        
        function updateNotificacao(notificacaoVO, fRegister) {
        	notificacoesService.updateNotificacao(notificacaoVO).then(function(data, status, headers, config) {
                $('#RegisterModal_notificacao').modal('hide');
                vm.resetForm(fRegister);
            });
        }
        
        function resetForm(fRegister) {
            vm.initNotificacao();
            fRegister.$setUntouched();
            fRegister.$setPristine();
            notificacoesService.setError();
        }

        function exibirDados(notificacao) {
        	notificacao.data = new Date(notificacao.data);
            vm.notificacao = notificacao;
        }

        function showError() {
            return notificacoesService.getError();
        }

        //inicializações
        vm.initNotificacao();
        vm.getAllValidNotificacao();
        
    }
})();