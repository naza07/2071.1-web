(function() {
	'use strict';
	console.log("[ usuariosController -  Controlador iniciado ]");

	angular.module("avaliacao").controller('usuariosController', usuariosController);

	usuariosController.$inject = [ 'usuariosService' ];

	function usuariosController(usuariosService) {
		var vm = this;

		// variáveis
		vm.usuarioResponsavel = null;
		vm.usuarioAluno = null;
		vm.usuarioProfessor = null;

		vm.senhaConf = "";
		vm.usuarioList = null;
		vm.continuarCadastro = false;

		// funções
		vm.getAllValidUsers = getAllValidUsers;
		vm.deleteUser = deleteUser;

		vm.initUserResponsavel = initUserResponsavel;
		vm.resetFormResponsavel = resetFormResponsavel;
		vm.confimacaoSenhaResponsavel = confimacaoSenhaResponsavel;

		vm.saveUserResponsavel = saveUserResponsavel;
		vm.updateUserResponsavel = updateUserResponsavel;

		vm.initUserProfessor = initUserProfessor;
		vm.resetFormProfessor = resetFormProfessor;
		vm.confimacaoSenhaProfessor = confimacaoSenhaProfessor;

		vm.saveUserProfessor = saveUserProfessor;
		vm.updateUserProfessor = updateUserProfessor;

		vm.initUserAluno = initUserAluno;
		vm.resetFormAluno = resetFormAluno;
		vm.confimacaoSenhaAluno = confimacaoSenhaAluno;

		vm.saveUserAluno = saveUserAluno;
		vm.updateUserAluno = updateUserAluno;

		vm.exibirDadosResponsavel = exibirDadosResponsavel;
		vm.showErrorResponsavel = showErrorResponsavel;

		vm.exibirDadosAluno = exibirDadosAluno;
		vm.showErrorAluno = showErrorAluno;

		vm.exibirDadosProfessor = exibirDadosProfessor;
		vm.showErrorProfessor = showErrorProfessor;
		
		vm.exportData = exportData;
		
		vm.exportPdf = exportPdf;
		
		function exportPdf(){
		     html2canvas(document.getElementById('exportable'), {
		         onrendered: function(canvas) {
		           var data = canvas.toDataURL();
		           var docDefinition = {
		             content: [{
		               image: data,
		               width: 500,
		             }]
		           };
		           pdfMake.createPdf(docDefinition).download("usuarios.pdf");
		         }
		       });
		
		}
		
		function exportData() {
	        var blob = new Blob([document.getElementById('exportable').innerHTML], {
	            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8"
	        });
	        saveAs(blob, "Report.xls");
		}

		function getAllValidUsers() {
			usuariosService.getAllValidUsers().then(
					function(data, status, headers, config) {
						vm.usuarioList = data.data;

					});
		}
		
		function deleteUser(idUsuario, usuarioVO) {
			usuariosService.deleteUser(idUsuario).then(
					function(data, status, headers, config) {
						var index = vm.usuarioList.indexOf(usuarioVO);
						vm.usuarioList.splice(index, 1);
					});
		}


		// Usuário Responsável
		function initUserResponsavel() {
			vm.usuarioResponsavel = {
				id_usuario : 0,
				nome : "",
				email : "",
				cpf : "",
				senha : "",
				tipoUsuarioIdTipoUsuario : 0,
			};
		}

		function saveUserResponsavel(usuarioVO, fRegister) {
			usuariosService.saveUserResponsavel(usuarioVO).then(
					function(data, status, headers, config) {
						usuarioVO.tipoUsuarioIdTipoUsuario = 3;
						vm.usuarioList.push(usuarioVO);
						if (!vm.continuarCadastro) {
							$('#RegisterModal_responsavel').modal('hide');
						}
					});
			console.log("OK");
			resetFormResponsavel(fRegister);
			console.log(vm.usuarioResponsavel.cpf);
		}

		function updateUserResponsavel(usuarioVO, fRegister) {
			usuariosService.updateUserResponsavel(usuarioVO).then(
					function(data, status, headers, config) {
						$('#RegisterModal_responsavel').modal('hide');
						vm.resetFormResponsavel(fRegister);
					});
		}

		function confimacaoSenhaResponsavel(fRegister) {
			if (vm.usuarioResponsavel.senha != vm.senhaConf) {
				fRegister.senhaConf.$setValidity("senhaConfError", false);
			} else {
				fRegister.senhaConf.$setValidity("senhaConfError", true);
			}
		}

		function resetFormResponsavel(fRegister) {
	
			vm.initUserResponsavel();
			vm.senhaConf = "";
			fRegister.$setUntouched();
			fRegister.$setPristine();
			usuariosService.setError();
		}

		function exibirDadosResponsavel(usuarioVO) {
			vm.usuarioResponsavel = usuarioVO;
		}

		function showErrorResponsavel() {
			return usuariosService.getError();
		}

		// Usuário Professor
		function initUserProfessor() {
			vm.usuarioProfessor = {
				id_usuario : 0,
				nome : "",
				email : "",
				cpf : "",
				senha : "",
				tipoUsuarioIdTipoUsuario : 0,
			};
		}

		function saveUserProfessor(usuarioVO, fRegister) {
			usuariosService.saveUserProfessor(usuarioVO).then(
					function(data, status, headers, config) {
						usuarioVO.tipoUsuarioIdTipoUsuario = 2;
						vm.usuarioList.push(usuarioVO);
						if (!vm.continuarCadastro) {
							$('#RegisterModal_professor').modal('hide');
						}
					});
			resetFormProfessor(fRegister);
		}

		function updateUserProfessor(usuarioVO, fRegister) {
			usuariosService.updateUserProfessor(usuarioVO).then(
					function(data, status, headers, config) {
						$('#RegisterModal_professor').modal('hide');
						vm.resetFormProfessor(fRegister);
					});
		}

		function confimacaoSenhaProfessor(fRegister) {
			if (vm.usuarioProfessor.senha != vm.senhaConf) {
				fRegister.senhaConf.$setValidity("senhaConfError", false);
			} else {
				fRegister.senhaConf.$setValidity("senhaConfError", true);
			}
		}

		function resetFormProfessor(fRegister) {
			vm.initUserProfessor();
			vm.senhaConf = "";
			fRegister.$setUntouched();
			fRegister.$setPristine();
			usuariosService.setError();
		}

		function exibirDadosProfessor(usuarioVO) {
			vm.usuarioProfessor = usuarioVO;
		}

		function showErrorProfessor() {
			return usuariosService.getError();
		}

		// Usuário Aluno
		function initUserAluno() {
			vm.usuarioAluno = {
				id_usuario : 0,
				nome : "",
				email : "",
				cpf : "",
				senha : "",
				tipoUsuarioIdTipoUsuario : 0,
				nascimento : "",
				escolaridade : "",
				escola : "",
				cidade : "",
				bairro : "",
			};
		}

		function saveUserAluno(usuarioVO, fRegister) {
			usuariosService.saveUserAluno(usuarioVO).then(
					function(data, status, headers, config) {
						usuarioVO.tipoUsuarioIdTipoUsuario = 1;
						vm.usuarioList.push(usuarioVO);
						if (!vm.continuarCadastro) {
							$('#RegisterModal_aluno').modal('hide');
						}
					});
			resetFormAluno(fRegister);
		}

	
		function updateUserAluno(usuarioVO, fRegister) {
			usuariosService.updateUserAluno(usuarioVO).then(
					function(data, status, headers, config) {
						$('#RegisterModal_aluno').modal('hide');
						vm.resetFormAluno(fRegister);
					});
		}

		function confimacaoSenhaAluno(fRegister) {
			if (vm.usuarioAluno.senha != vm.senhaConf) {
				fRegister.senhaConf.$setValidity("senhaConfError", false);
			} else {
				fRegister.senhaConf.$setValidity("senhaConfError", true);
			}
		}

		function resetFormAluno(fRegister) {
			vm.initUserAluno();
			vm.senhaConf = "";
			fRegister.$setUntouched();
			fRegister.$setPristine();
			usuariosService.setError();
		}

		function exibirDadosAluno(usuarioVO) {
			vm.usuarioAluno = usuarioVO;
		}

		function showErrorAluno() {
			return usuariosService.getError();
		}

		// inicializações
		vm.initUserResponsavel();
		vm.initUserProfessor();
		vm.initUserAluno();
		vm.getAllValidUsers();
		
		//console.log(vm.usuarioResponsavel);

	}
})();