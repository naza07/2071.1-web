(function() {
    'use strict';
    console.log("[ materialController -  Controlador iniciado ]");

    angular.module("avaliacao").controller('materialController', materialController);

    materialController.$inject = ['materialService'];

    function materialController(materialService) {
        var vm = this;
        
        //variaveis
        vm.material = null;
        vm.materialList = null;
        vm.continuarCadastro = false;
        
        //funções
        vm.initMaterial = initMaterial;
        vm.getAllValidMaterial = getAllValidMaterial;
        vm.saveMaterial = saveMaterial;
        vm.deleteMaterial = deleteMaterial;
        vm.updateMaterial = updateMaterial;
        vm.showError = showError;
        vm.resetForm = resetForm;
        vm.exibirDados = exibirDados;
        
        function initMaterial(){
        	vm.material = {
        			idmaterial: 0,
        			titulo: "",
        			descricao: "",
        			data: "",
        			link: ""
        	};
        }
        
        function getAllValidMaterial() {
            materialService.getAllValidMaterial().then(function(data, status, headers, config) {
                vm.materialList = data.data;
                console.log(vm.materialList);
            });
        }
        

        function saveMaterial(material, fRegister) {
            materialService.saveMaterial(material).then(function(data, status, headers, config) {
                vm.materialList.push(material);
                if (!vm.continuarCadastro) {
                    $('#RegisterModal_material').modal('hide');
                }
            });
            resetForm(fRegister);
        }
        
        function deleteMaterial(idMaterial, material) {
            materialService.deleteMaterial(idMaterial).then(function(data, status, headers, config) {
                var index = vm.materialList.indexOf(material);
                vm.materialList.splice(index, 1);
            });
        }
        
        function updateMaterial(avaliacaoUsuarioVO, fRegister) {
            materialService.updateMaterial(avaliacaoUsuarioVO).then(function(data, status, headers, config) {
                $('#RegisterModal_material').modal('hide');
                vm.resetForm(fRegister);
            });
        }
        
    	function resetForm(fRegister) {
            vm.initMaterial();
            fRegister.$setUntouched();
            fRegister.$setPristine();
            materialService.setError();
        }

        function exibirDados(material) {
            vm.material = material;
        }

        function showError() {
            return materialService.getError();
        }

        //inicializações
        vm.initMaterial();
        vm.getAllValidMaterial();

    }
})();