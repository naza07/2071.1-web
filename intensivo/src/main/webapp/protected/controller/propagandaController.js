(function() {
    'use strict';
    console.log("[ propagandaController -  Controlador iniciado ]");

    angular.module("avaliacao").controller('propagandaController', propagandaController);

    propagandaController.$inject = ['propagandaService'];

    function propagandaController(propagandaService) {
        var vm = this;
        
        //variaveis
        vm.propaganda = null;
        vm.propagandaList = null;
        
        //funções
        vm.initPropaganda = initPropaganda;
        vm.getAllValidPropaganda = getAllValidPropaganda;
        vm.savePropaganda = savePropaganda;
        vm.deletePropaganda = deletePropaganda;
        vm.updatePropaganda = updatePropaganda;
        vm.showError = showError;
        vm.resetForm = resetForm;
        vm.exibirDados = exibirDados;
        
        function initPropaganda(){
        	vm.propaganda = {
        			id_propaganda: 0,
        			imagem_url: "",
        			nome: "",
        			data: "",
        			
        	};
        }
        
        function getAllValidPropaganda() {
            propagandaService.getAllValidPropaganda().then(function(data, status, headers, config) {
                vm.propagandaList = data.data;
            });
        }
        
        function savePropaganda(propaganda, fRegister) {
            propagandaService.savePropaganda(propaganda).then(function(data, status, headers, config) {
                vm.propagandaList.push(propaganda);
                if (!vm.continuarCadastro) {
                    $('#RegisterModal_propaganda').modal('hide');
                }
            });
            resetForm(fRegister);
        }
        
        function deletePropaganda(idPropaganda, propaganda) {
        	propagandaService.deletePropaganda(idPropaganda).then(function(data, status, headers, config) {
                var index = vm.propagandaList.indexOf(propaganda);
                vm.propagandaList.splice(index, 1);
            });
        }
        
        function updatePropaganda(propagandaVO, fRegister) {
        	propagandaService.updatePropaganda(propagandaVO).then(function(data, status, headers, config) {
                $('#RegisterModal_propaganda').modal('hide');
                vm.resetForm(fRegister);
            });
        }
        
     	function resetForm(fRegister) {
            vm.initPropaganda();
            fRegister.$setUntouched();
            fRegister.$setPristine();
            propagandaService.setError();
        }
     	
     	function exibirDados(propaganda) {
     		propaganda.data = new Date(propaganda.data);
            vm.propaganda = propaganda;
        }
     	
        function showError() {
            return propagandaService.getError();
        }
        
        
        vm.initPropaganda();
        vm.getAllValidPropaganda();
        
    }
})();