(function() {
	'use strict';
	console.log("[ homeController -  Controlador iniciado ]");

	angular.module("avaliacao").controller('homeController', homeController);
	
	homeController.$inject = [ 'usuariosService', 'materialService', 'notificacoesService', 'propagandaService' ];

	function homeController(usuariosService, materialService, notificacoesService, propagandaService) {
		var vm = this;
		
		//variaveis
		vm.prof = [];
		vm.alun = [];
		vm.resp = [];
		
		vm.meses = { 
				"mat":{
					"jan": 0,
					"fev": 0,
					"mar": 0,
					"abr" : 0,
					"mai" : 0,
					"jun" : 0,
					"jul" : 0,
					"ago" : 0,
					"set" : 0,
					"out" : 0,
					"nov" : 0,
					"dez" : 0
			},
			"not":{
				"jan": 0,
				"fev": 0,
				"mar": 0,
				"abr" : 0,
				"mai" : 0,
				"jun" : 0,
				"jul" : 0,
				"ago" : 0,
				"set" : 0,
				"out" : 0,
				"nov" : 0,
				"dez" : 0
			},
			"prop":{
				"jan": 0,
				"fev": 0,
				"mar": 0,
				"abr" : 0,
				"mai" : 0,
				"jun" : 0,
				"jul" : 0,
				"ago" : 0,
				"set" : 0,
				"out" : 0,
				"nov" : 0,
				"dez" : 0
			},
			"hor":{
				"jan": 0,
				"fev": 0,
				"mar": 0,
				"abr" : 0,
				"mai" : 0,
				"jun" : 0,
				"jul" : 0,
				"ago" : 0,
				"set" : 0,
				"out" : 0,
				"nov" : 0,
				"dez" : 0
			}
		}
		
		//console.log(vm.meses.hor.nov);
		
		vm.colors = ['#45b7cd', '#ff6384', '#ff8e72'];
	    vm.labels = ['Professores', 'Alunos', 'Responsaveis'];
	    
	    //mat
	    vm.labels_mat = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez' ];
	    vm.series_mat = ['Meses'];
	    vm.data_mat = [vm.meses.mat.jan, vm.meses.mat.fev, vm.meses.mat.mar, vm.meses.mat.abr, vm.meses.mat.mai, vm.meses.mat.jun,
	    	vm.meses.mat.jul, vm.meses.mat.ago, vm.meses.mat.set, vm.meses.mat.out, vm.meses.mat.nov, vm.meses.mat.dez];
	    
	    //not
	    vm.labels_not = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez' ];
	    vm.series_not = ['Meses'];
	    vm.data_not = [vm.meses.not.jan, vm.meses.not.fev, vm.meses.not.mar, vm.meses.not.abr, vm.meses.not.mai, vm.meses.not.jun,
	    	vm.meses.not.jul, vm.meses.not.ago, vm.meses.not.set, vm.meses.not.out, vm.meses.not.nov, vm.meses.not.dez];
	    
	    //hor
	    vm.labels_hor = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez' ];
	    vm.series_hor = ['Meses'];
	    vm.data_hor = [vm.meses.hor.jan, vm.meses.hor.fev, vm.meses.hor.mar, vm.meses.hor.abr, vm.meses.hor.mai, vm.meses.hor.jun,
	    	vm.meses.hor.jul, vm.meses.hor.ago, vm.meses.hor.set, vm.meses.hor.out, vm.meses.hor.nov, vm.meses.hor.dez];
	    
	    //prop
	    vm.labels_prop = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez' ];
	    vm.series_prop = ['Meses'];
	    vm.data_prop = [vm.meses.prop.jan, vm.meses.prop.fev, vm.meses.prop.mar, vm.meses.prop.abr, vm.meses.prop.mai, vm.meses.prop.jun,
	    	vm.meses.prop.jul, vm.meses.prop.ago, vm.meses.prop.set, vm.meses.prop.out, vm.meses.prop.nov, vm.meses.prop.dez];
	    
		// funções
		vm.getAllValidUsers = getAllValidUsers;
		vm.getAllValidMaterial = getAllValidMaterial;
		vm.getAllValidPropaganda = getAllValidPropaganda;
		vm.getAllValidNotificacao = getAllValidNotificacao;
		vm.initValues = initValues;
	    
	    function getAllValidUsers() {
			usuariosService.getAllValidUsers().then(
				function(data, status, headers, config) {
					for(var i = 0; i < data.data.length; i++){
						if(data.data[i].tipoUsuarioIdTipoUsuario == 1){
							vm.resp.push(data.data[i]);
						}else if(data.data[i].tipoUsuarioIdTipoUsuario == 2){
							vm.prof.push(data.data[i]);
						}else{
							vm.alun.push(data.data[i]);
						}
					}
					vm.initValues();
				}
			);
		}
	    
	    function getAllValidMaterial() {
            materialService.getAllValidMaterial().then(
            		function(data, status, headers, config) {
            			var jan = 0 ,fev = 0,mar = 0,abr = 0,mai = 0,jun = 0,jul = 0,ago = 0,set = 0,out = 0,nov = 0,dez = 0;
            			for(var i = 0; i < data.data.length; i++){
                        	var date = moment(data.data[0].data).format("DD MM YYYY");
                        	var mes = date.substring(3, 5);
                        	if(mes == "01"){
                        		jan++;
                        	}else if(mes == "02"){
                        		fev++;
                        	}else if(mes == "03"){
                        		mar++;
                        	}else if(mes == "04"){
                        		abr++;
                        	}else if(mes == "05"){
                        		mai++;
                        	}else if(mes == "06"){
                        		jun++;
                        	}else if(mes == "07"){
                        		jul++;
                        	}else if(mes == "08"){
                        		ago++;
                        	}else if(mes == "09"){
                        		set++;
                        	}else if(mes == "10"){
                        		out++;
                        	}else if(mes == "11"){
                        		nov++;
                        	}else if(mes == "12"){
                        		dez++;
                        	}
                        	
                        }
            			
            			vm.meses.mat.jan = jan;
            			vm.meses.mat.fev = fev;
            			vm.meses.mat.mar = mar;
            			vm.meses.mat.abr = abr;
            			vm.meses.mat.mai = mai;
            			vm.meses.mat.jun = jun;
            			vm.meses.mat.jul = jul;
            			vm.meses.mat.ago = ago;
            			vm.meses.mat.set = set;
            			vm.meses.mat.out = out;
            			vm.meses.mat.nov = nov;
            			vm.meses.mat.dez = dez;
            			
            			initValues();
            		}
            );
        }
	    
	    function getAllValidNotificacao() {  	
        	notificacoesService.getAllValidNotificacao().then(function(data, status, headers, config) {
        		var jan = 0 ,fev = 0,mar = 0,abr = 0,mai = 0,jun = 0,jul = 0,ago = 0,set = 0,out = 0,nov = 0,dez = 0;
    			for(var i = 0; i < data.data.length; i++){
                	var date = moment(data.data[0].data).format("DD MM YYYY");
                	var mes = date.substring(3, 5);
                	if(mes == "01"){
                		jan++;
                	}else if(mes == "02"){
                		fev++;
                	}else if(mes == "03"){
                		mar++;
                	}else if(mes == "04"){
                		abr++;
                	}else if(mes == "05"){
                		mai++;
                	}else if(mes == "06"){
                		jun++;
                	}else if(mes == "07"){
                		jul++;
                	}else if(mes == "08"){
                		ago++;
                	}else if(mes == "09"){
                		set++;
                	}else if(mes == "10"){
                		out++;
                	}else if(mes == "11"){
                		nov++;
                	}else if(mes == "12"){
                		dez++;
                	}
                	
                }
    			
    			vm.meses.not.jan = jan;
    			vm.meses.not.fev = fev;
    			vm.meses.not.mar = mar;
    			vm.meses.not.abr = abr;
    			vm.meses.not.mai = mai;
    			vm.meses.not.jun = jun;
    			vm.meses.not.jul = jul;
    			vm.meses.not.ago = ago;
    			vm.meses.not.set = set;
    			vm.meses.not.out = out;
    			vm.meses.not.nov = nov;
    			vm.meses.not.dez = dez;
    			
    			initValues();
    		
            });
        }
	    
	    function getAllValidPropaganda() {
            propagandaService.getAllValidPropaganda().then(function(data, status, headers, config) {
        		var jan = 0 ,fev = 0,mar = 0,abr = 0,mai = 0,jun = 0,jul = 0,ago = 0,set = 0,out = 0,nov = 0,dez = 0;
    			for(var i = 0; i < data.data.length; i++){
                	var date = moment(data.data[0].data).format("DD MM YYYY");
                	var mes = date.substring(3, 5);
                	if(mes == "01"){
                		jan++;
                	}else if(mes == "02"){
                		fev++;
                	}else if(mes == "03"){
                		mar++;
                	}else if(mes == "04"){
                		abr++;
                	}else if(mes == "05"){
                		mai++;
                	}else if(mes == "06"){
                		jun++;
                	}else if(mes == "07"){
                		jul++;
                	}else if(mes == "08"){
                		ago++;
                	}else if(mes == "09"){
                		set++;
                	}else if(mes == "10"){
                		out++;
                	}else if(mes == "11"){
                		nov++;
                	}else if(mes == "12"){
                		dez++;
                	}
                	
                }
    			
    			vm.meses.prop.jan = jan;
    			vm.meses.prop.fev = fev;
    			vm.meses.prop.mar = mar;
    			vm.meses.prop.abr = abr;
    			vm.meses.prop.mai = mai;
    			vm.meses.prop.jun = jun;
    			vm.meses.prop.jul = jul;
    			vm.meses.prop.ago = ago;
    			vm.meses.prop.set = set;
    			vm.meses.prop.out = out;
    			vm.meses.prop.nov = nov;
    			vm.meses.prop.dez = dez;
    			
    			initValues();
            });
        }
	    	    
	    function initValues(){
		    vm.data = [vm.prof.length, vm.alun.length, vm.resp.length];
		    vm.data_mat = [vm.meses.mat.jan, vm.meses.mat.fev, vm.meses.mat.mar, vm.meses.mat.abr, vm.meses.mat.mai, vm.meses.mat.jun,
		    	vm.meses.mat.jul, vm.meses.mat.ago, vm.meses.mat.set, vm.meses.mat.out, vm.meses.mat.nov, vm.meses.mat.dez];
		    vm.data_not = [vm.meses.not.jan, vm.meses.not.fev, vm.meses.not.mar, vm.meses.not.abr, vm.meses.not.mai, vm.meses.not.jun,
		    	vm.meses.not.jul, vm.meses.not.ago, vm.meses.not.set, vm.meses.not.out, vm.meses.not.nov, vm.meses.not.dez];
		    vm.data_hor = [vm.meses.hor.jan, vm.meses.hor.fev, vm.meses.hor.mar, vm.meses.hor.abr, vm.meses.hor.mai, vm.meses.hor.jun,
		    	vm.meses.hor.jul, vm.meses.hor.ago, vm.meses.hor.set, vm.meses.hor.out, vm.meses.hor.nov, vm.meses.hor.dez];
		    vm.data_prop = [vm.meses.prop.jan, vm.meses.prop.fev, vm.meses.prop.mar, vm.meses.prop.abr, vm.meses.prop.mai, vm.meses.prop.jun,
		    	vm.meses.prop.jul, vm.meses.prop.ago, vm.meses.prop.set, vm.meses.prop.out, vm.meses.prop.nov, vm.meses.prop.dez];
	    }
	    
	    //inicializações
	    vm.getAllValidUsers();
	    vm.getAllValidMaterial();
	    vm.getAllValidNotificacao();
	    vm.getAllValidPropaganda();
		
	}
})();