<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="col-sm-12" ng-init="vm.selectedTab = 'hor'" ng-controller="horarioController as vm">
	<div class="row">
		<div class="col-sm-12 text-center spacing" style="margin-top: -20px;">
			<h1>Consultar Horários</h1>
		</div>
	</div>

	<div class="row spacing">
		<div class="col-sm-12 pull-left">
			<button type="button" class="btn btn-primary" data-toggle="modal"
				data-target="#RegisterModal_horario">
				<span class="glyphicon glyphicon-plus"></span>&nbsp;Adicionar
				Horário
			</button>
		</div>
	</div>
	
	<div class="row">
		<table class="table table-hover">
			<thead>
				<tr>
					<th class="col-xs-3">Dia da semana</th>
					<th class="col-xs-2">Hora inicio</th>
					<th class="col-xs-2">Hora fim</th>
					<th class="col-xs-3">Materia</th>
					<th class="col-xs-2">Ações</th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="horario in vm.horarioList">
					<td class="col-xs-3">{{horario.dia}}</td>
					<td class="col-xs-2">{{horario.inicio}}</td>
					<td class="col-xs-2">{{horario.fim}}</td>
					<td class="col-xs-3">{{horario.materia}}</td>
					<td class="col-xs-2">
						<button type="submit" class="btn btn-success btn-sm"
						ng-click="vm.exibirDados(horario)"
							data-toggle="modal" data-target="#RegisterModal_horario">
							<span class="glyphicon glyphicon-pencil"></span>&nbsp;Editar
						</button>
						<button type="button" class="btn btn-danger btn-sm"
								ng-click="vm.deleteHorario(horario.idHorario, horario)">
							<span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;Excluir
						</button>
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="row">
		<!-- <div class="col-xs-12">
			<div id="calendar" ui-calendar="vm.uiConfig.calendar"
				ng-model="vm.eventSources" calendar="myCalendar"></div>
		</div> -->

		<!-- cadastro material Modal -->
		<div class="row">

			<div name="RegisterModal_horario" id="RegisterModal_horario"
				class="modal fade bd-modal-sm" tabindex="-1" role="dialog"
				aria-labelledby="RegisterModalLabel" aria-hidden="true"
				data-backdrop="static" data-keyboard="false">
				<div class="modal-dialog modal-sm">
					<div class="modal-content">

						<!-- formulario cadastro -->
						<form name="fRegister" id="fRegister"
							ng-controller="utilsController as uc">
							<!-- <div class="col-lg-1 col-md-1 col-sm-2 col-xs-0"></div> -->
							<div class="col-xs-12">

								<div class="text-center well">
									<h2 id="login">Cadastro de novos horários</h2>
									<hr />

									<div class="form-group">
										<label class="pull-left" for="idDias">Dias:</label> 
										<select class="form-control" id="idDias" name="dias" ng-model="vm.horario.dia">
											<option>Segunda</option>
											<option>Terça</option>
											<option>Quarta</option>
											<option>Quinta</option>
											<option>Sexta</option>
											<option>Sábado</option>
											<option>Domingo</option>
										</select>
										<div
											ng-show="fRegister.$submitted || fRegister.dias.$touched">
											<div ng-show="fRegister.dias.$error.required">
												<p class="wrong">O dia é obrigatório.</p>
											</div>
										</div>
									</div>

									<div class="form-group">
										<div class="col-xs-12 input-group">
											<label class="pull-left" for="idHoraInicio">Horário inicial:</label> 
											<input id="idHoraInicio" name="inicio"
												ng-model="vm.horario.inicio" type="time"
												class="form-control" required>
										</div>
										<div
											ng-show="fRegister.$submitted || fRegister.inicio.$touched">
											<div ng-show="fRegister.inicio.$error.required">
												<p class="wrong">A data de inicio é obrigatória.</p>
											</div>
										</div>
									</div>

									<div class="form-group">
										<div class="col-xs-12 input-group">
											<label class="pull-left" for="idHoraFim">Horário final:</label>
											<input id="idHoraFim" name="fim" ng-model="vm.horario.fim"
												type="time" class="form-control" required>
										</div>
										<div ng-show="fRegister.$submitted || fRegister.fim.$touched">
											<div ng-show="fRegister.fim.$error.required">
												<p class="wrong">A data final é obrigatória.</p>
											</div>
										</div>
									</div>

									<div class="form-group">
										<div class="col-xs-12 input-group">
											<label class="pull-left" for="idMaterias">Matéria:</label> 
											<select class="form-control" id="idMaterias" name="materia" ng-model="vm.horario.materia">
												<option value="1">Portugues (1)</option>
												<option value="2">Matematica (2)</option>
												<option value="3">Ciencias (3)</option>
												<option value="4">Historia (4)</option>
												<option value="5">Geografia (5)</option>
											</select>
										</div>
										<div ng-show="fRegister.$submitted || fRegister.materia.$touched">
											<div ng-show="fRegister.materia.$error.required">
												<p class="wrong">A matéria é obrigatória.</p>
											</div>
										</div>
									</div>

									<div ng-if="vm.horario.idHorario == 0">
										<div class="form-group">
											<div class="col-xs-12 input-group">
												<label class="checkbox-inline pull-left"> <input
													type="checkbox" value="" ng-model="vm.continuarCadastro">Continuar
													cadastrando?
												</label>
											</div>
										</div>
									</div>

									<div ng-show="vm.showError() != ''">
										<div class="text-center alert alert-danger">
											{{vm.showError()}}</div>
									</div>

									<div ng-show="uc.control.bol">
										<div class="text-center alert alert-warning">
											{{uc.control.error}}</div>
									</div>

									<div class="form-group">
										<div class="col-xs-12 input-group">
											<div class="col-xs-6 pull-left">
												<div ng-if="vm.horario.idHorario == 0">
													<button name="alterar" type="submit"
														class="btn btn-primary" ng-disabled="fRegister.$invalid"
														ng-click="vm.saveHorario(vm.horario, fRegister)">
														<span class="glyphicon glyphicon-floppy-save"></span>&nbsp;
														Cadastrar
													</button>
												</div>
												<div ng-if="vm.horario.idHorario != 0">
													<button name="alterar" type="submit"
														class="btn btn-primary" ng-disabled="fRegister.$invalid"
														ng-click="vm.updateHorario(vm.horario, fRegister)">
														<span class="glyphicon glyphicon-floppy-save"></span>&nbsp;
														Alterar
													</button>
												</div>
											</div>
											<div class="col-xs-6 pull-right">
												<button type="button" class="btn btn-danger"
													ng-click="vm.resetForm(fRegister)" data-dismiss="modal">
													<span class="glyphicon glyphicon-remove"></span>&nbsp;
													Fechar
												</button>
											</div>
										</div>
									</div>

								</div>
							</div>
							<!-- <div class="col-lg-1 col-md-1 col-sm-2 col-xs-0"></div> -->
						</form>
						<!-- fim formulario -->

					</div>
				</div>
			</div>
		</div>
		<!-- fim modal -->
	</div>

	<script src="../protected/controller/horarioController.js"></script>
	<script src="../protected/service/horarioService.js"></script>
	<script src="../resources/js/utilsController.js"></script>