<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="col-sm-12" ng-controller="notificacoesController as vm"
	ng-init="vm.selectedTab='notf'">
	<div class="row">
		<div class="col-sm-12 text-center spacing">
			<h1>Consultar Notificações</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-4">
			<button type="button" class="btn btn-primary" data-toggle="modal"
				data-target="#RegisterModal_notificacao">
				<span class="glyphicon glyphicon-plus"></span>&nbsp;Adicionar
				Notificação
			</button>
		</div>
		<div class="col-sm-offset-4 col-sm-4 spacing">
			<div class="inner-addon right-addon">
				<i class="glyphicon glyphicon-search"></i> <input type="text"
					class="form-control" placeholder="Procurar notificações"
					ng-model="procurar_n" />
			</div>
		</div>
	</div>

	<div class="row">
		<table class="table table-hover">
			<thead>
				<tr>
					<th class="col-xs-3">Título</th>
					<th class="col-xs-3">Descrição</th>
					<th class="col-xs-3">Data</th>
					<th class="col-xs-3">Ações</th>
				</tr>
			</thead>
			<tbody ng-repeat="notificacao in vm.notificacaoList | filter:notificacao.titulo = procurar_n">
				<tr>
				<td class="col-xs-3">{{notificacao.titulo}}
				</td>
							<td class="col-xs-3">{{notificacao.descricao}}</td>
							<td class="col-xs-3">{{notificacao.data | date : "dd/MM/yyyy"}}</td>
							<td class="col-xs-3">
								<button type="submit" class="btn btn-success btn-sm" 
									ng-click="vm.exibirDados(notificacao)"
									data-toggle="modal" data-target="#RegisterModal_notificacao">
									<span class="glyphicon glyphicon-pencil"></span>&nbsp;Editar
								</button>
								<button type="button" class="btn btn-danger btn-sm"
										ng-click="vm.deleteNotificacao(notificacao.idnotificacao, notificacao)">
									<span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;Excluir
								</button>
							</td>
						</tr>
			</tbody>
		</table>
	</div>
	
	<!-- cadastro notificacao Modal -->
	<div class="row">

		<div name="RegisterModal_notificacao" id="RegisterModal_notificacao" class="modal fade bd-modal-lg" tabindex="-1" role="dialog"
			aria-labelledby="RegisterModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">

					<!-- formulario cadastro -->
					<form name="fRegister" id="fRegister" ng-controller="utilsController as uc">
						<!-- <div class="col-lg-1 col-md-1 col-sm-2 col-xs-0"></div> -->
						<div class="col-xs-12">

							<div class="text-center well">
								<h2 id="login">Cadastro de novas notificações</h2>
								<hr />

								<div class="form-group">
									<div class="col-xs-12 input-group">
										<span class="input-group-addon">
											<i class="glyphicon glyphicon-book"></i>
										</span> 
										<input id="idTitulo" name="titulo" ng-model="vm.notificacao.titulo"
											type="Text" class="form-control" placeholder="Título Completo"
											required>
									</div>
									<div ng-show="fRegister.$submitted || fRegister.titulo.$touched">
										<div ng-show="fRegister.titulo.$error.required">
											<p class="wrong">O titulo é obrigatório.</p>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="col-xs-12 input-group">
										<span class="input-group-addon"> 
											<i class="glyphicon glyphicon-comment"></i>
										</span>
											<input id="idDescr" name="descricao"
											ng-model="vm.notificacao.descricao" type="text"
											class="form-control" placeholder="Descrição da notificação" required>
									</div>
									<div
										ng-show="fRegister.$submitted || fRegister.descricao.$touched">
										<div ng-show="fRegister.descricao.$error.required">
											<p class="wrong">A descrição é obrigatória.</p>
										</div>
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-xs-12 input-group">
										<span class="input-group-addon"> <i
											class="glyphicon glyphicon-calendar"></i>
										</span> 
										<input id="idData" name="data" ng-model="vm.notificacao.data"
											type="Date" class="form-control" required>
									</div>
									<div ng-show="fRegister.$submitted || fRegister.data.$touched">
										<div ng-show="fRegister.data.$error.required">
											<p class="wrong">A data é obrigatória.</p>
										</div>
									</div>
								</div>

								<div ng-if="vm.notificacao.idnotificacao == 0">
									<div class="form-group">
										<div class="col-xs-12 input-group">
											<label class="checkbox-inline pull-left"> <input
												type="checkbox" value="" ng-model="vm.continuarCadastro">Continuar
												cadastrando?
											</label>
										</div>
									</div>
								</div>

								<div ng-show="vm.showError() != ''">
									<div class="text-center alert alert-danger">
										{{vm.showError()}}</div>
								</div>

								<div ng-show="uc.control.bol">
									<div class="text-center alert alert-warning">
										{{uc.control.error}}</div>
								</div>

								<div class="form-group">
									<div class="col-xs-12 input-group">
										<div class="col-xs-6 pull-left">
											<div ng-if="vm.notificacao.idnotificacao == 0">
												<button name="alterar" type="submit" class="btn btn-primary"
													ng-disabled="fRegister.$invalid"
													ng-click="vm.saveNotificacao(vm.notificacao, fRegister)">
													<span class="glyphicon glyphicon-floppy-save"></span>&nbsp;
													Cadastrar
												</button>
											</div>
											<div ng-if="vm.notificacao.idnotificacao != 0">
												<button name="alterar" type="submit" class="btn btn-primary"
													ng-disabled="fRegister.$invalid"
													ng-click="vm.updateNotificacao(vm.notificacao, fRegister)">
													<span class="glyphicon glyphicon-floppy-save"></span>&nbsp;
													Alterar
												</button>
											</div>
										</div>
										<div class="col-xs-6 pull-right">
											<button type="button" class="btn btn-danger"
												ng-click="vm.resetForm(fRegister)" data-dismiss="modal">
												<span class="glyphicon glyphicon-remove"></span>&nbsp;
												Fechar
											</button>
										</div>
									</div>
								</div>

							</div>
						</div>
						<!-- <div class="col-lg-1 col-md-1 col-sm-2 col-xs-0"></div> -->
					</form>
					<!-- fim formulario -->

				</div>
			</div>
		</div>
	</div>
	<!-- fim modal -->
</div>
<script src="../protected/controller/notificacoesController.js"></script>
<script src="../protected/service/notificacoesService.js"></script>
<script src="../resources/js/utilsController.js"></script>
