<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="col-sm-12" ng-init="vm.selectedTab = 'users'"
	ng-controller="usuariosController as vm">

	<div class="row">
		<div class="col-sm-12 text-center spacing">
			<h1>Resumo dos Usuários</h1>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-2 pull-left">
			<button type="button" class="btn btn-primary" data-toggle="modal"
				data-target="#RegisterModal_professor">
				<span class="glyphicon glyphicon-education"></span>&#32;Cadastrar
				Professor
			</button>
		</div>
		<div class="col-sm-2 pull-left">
			<button type="button" class="btn btn-primary" data-toggle="modal"
				data-target="#RegisterModal_responsavel">
				<span class="glyphicon glyphicon-user"></span>&#32;Cadastrar
				Responsavel
			</button>
		</div>
		<div class="col-sm-2 pull-left">

			<button type="button" class="btn btn-primary" data-toggle="modal"
				data-target="#RegisterModal_aluno">
				<span class="glyphicon glyphicon-education"></span>&#32;Cadastrar
				Aluno
			</button>
		</div>
		<div class="col-sm-offset-3 col-sm-3 spacing">
			<div class="inner-addon right-addon">
				<i class="glyphicon glyphicon-search"></i> <input type="text"
					class="form-control" placeholder="Procurar" ng-model="procurar" />
			</div>
		</div>
	</div>

	<div class="col-sm-12">
		<div class="row " id="exportable">

			<table class="table table-hover export-table" id="">
				<thead>
					<tr>
						<th class="col-xs-3">Nome</th>
						<th class="col-xs-3">Email</th>
						<th class="col-xs-1">Tipo</th>
						<th class="col-xs-2">Ações</th>
					</tr>
				</thead>
				<tbody ng-repeat="usuario in vm.usuarioList | filter:usuario.nome = procurar  | orderBy: 'tipoUsuarioIdTipoUsuario'">
					<tr>
						<td class="col-xs-3">{{usuario.nome}}</td>
						<td class="col-xs-3">{{usuario.email}}</td>
						<td class="col-xs-1">{{usuario.tipoUsuarioIdTipoUsuario}}</td>
						<td class="col-xs-2">
							<button type="submit" class="btn btn-success btn-sm"
								ng-if="usuario.tipoUsuarioIdTipoUsuario == 1"
								ng-click="vm.exibirDadosAluno(usuario)"
								data-toggle="modal" data-target="#RegisterModal_aluno">
								<span class="glyphicon glyphicon-pencil"></span>&nbsp;Editar
							</button>
							<button type="submit" class="btn btn-success btn-sm"
								ng-if="usuario.tipoUsuarioIdTipoUsuario == 2"
								ng-click="vm.exibirDadosProfessor(usuario)"
								data-toggle="modal" data-target="#RegisterModal_professor">
								<span class="glyphicon glyphicon-pencil"></span>&nbsp;Editar
							</button>
							<button type="submit" class="btn btn-success btn-sm"
								ng-if="usuario.tipoUsuarioIdTipoUsuario == 3"
								ng-click="vm.exibirDadosResponsavel(usuario)"
								data-toggle="modal" data-target="#RegisterModal_responsavel">
								<span class="glyphicon glyphicon-pencil"></span>&nbsp;Editar
							</button>
							<button type="button" class="btn btn-danger btn-sm"
								ng-click="vm.deleteUser(usuario.id_usuario, usuario)">
								<span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;Excluir
							</button>
							
						</td>
					</tr>
				</tbody>
			</table>

		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<button type="submit" name="submit" class="btn btn-primary"
				ng-click="vm.exportPdf()">
				<span class="glyphicon glyphicon-download"></span>&nbsp;Gerar PDF
			</button>
			<button type="submit" name="submit" class="btn btn-primary"
				ng-click="vm.exportData()">
				<span class="glyphicon glyphicon-download"></span>&nbsp;Gerar
				Planilha
			</button>
		</div>
	</div>

	<!-- inicio dos modais -->

	<!-- cadastro aluno Modal -->
	<div class="row">

		<div name="RegisterModal_aluno" id="RegisterModal_aluno"
			class="modal fade bd-modal-lg" tabindex="-1" role="dialog"
			aria-labelledby="RegisterModalLabel" aria-hidden="true"
			data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">

					<!-- formulario cadastro -->
					<form name="fRegister" id="fRegister"
						ng-controller="utilsController as uc">
						<!-- <div class="col-lg-1 col-md-1 col-sm-2 col-xs-0"></div> -->
						<div class="col-xs-12">

							<div class="text-center well">
								<h2 id="login">Cadastro de novos aluno</h2>
								<hr />

								<div class="form-group">
									<div class="col-xs-12 input-group">
										<span class="input-group-addon"> <i
											class="glyphicon glyphicon-user"></i>
										</span> <input id="idNome" name="nome"
											ng-model="vm.usuarioAluno.nome" type="Text"
											class="form-control" placeholder="Nome completo" required>
									</div>
									<div ng-show="fRegister.$submitted || fRegister.nome.$touched">
										<div ng-show="fRegister.nome.$error.required">
											<p class="wrong">O nome Ã© obrigatÃ³rio.</p>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="col-xs-12 input-group">
										<span class="input-group-addon"> <i
											class="glyphicon glyphicon-user"></i> <!-- <label>Data de Nascimento:</label> -->
										</span> <input id="idNascimento" name="nascimento"
											ng-model="vm.usuarioAluno.nascimento" type="date"
											class="form-control" required>
									</div>
									<div
										ng-show="fRegister.$submitted || fRegister.nascimento.$touched">
										<div ng-show="fRegister.nascimento.$error.required">
											<p class="wrong">A data de nascimento Ã© obrigatÃ³ria.</p>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="col-xs-12 input-group">
										<span class="input-group-addon"> <i
											class="glyphicon glyphicon-user"></i>
										</span> <input id="idCPF" name="cpf" ng-model="vm.usuarioAluno.cpf"
											type="Text" class="form-control" placeholder="CPF" required>
									</div>
									<div ng-show="fRegister.$submitted || fRegister.cpf.$touched">
										<div ng-show="fRegister.cpf.$error.required">
											<p class="wrong">O CPF Ã© obrigatÃ³rio.</p>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="col-xs-12 input-group">
										<span class="input-group-addon"> <i
											class="glyphicon glyphicon-education"></i>
										</span> <input id="idEscolaridade" name="escolaridade"
											ng-model="vm.usuarioAluno.escolaridade" type="Text"
											class="form-control" placeholder="NÃ­vel de escolaridade"
											required>
									</div>
									<div
										ng-show="fRegister.$submitted || fRegister.escolaridade.$touched">
										<div ng-show="fRegister.escolaridade.$error.required">
											<p class="wrong">A escolaridade Ã© obrigatÃ³ria.</p>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="col-xs-12 input-group">
										<span class="input-group-addon"> <i
											class="glyphicon glyphicon-home"></i>
										</span> <input id="idEscola" name="escola"
											ng-model="vm.usuarioAluno.escola" type="Text"
											class="form-control" placeholder="Escola que estuda" required>
									</div>
									<div
										ng-show="fRegister.$submitted || fRegister.escola.$touched">
										<div ng-show="fRegister.escola.$error.required">
											<p class="wrong">O nome da escola Ã© obrigatÃ³ria.</p>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="col-xs-12 input-group">
										<span class="input-group-addon"> <i
											class="glyphicon glyphicon-envelope"></i>
										</span> <input id="idEmail" name="email" type="email"
											ng-model="vm.usuarioAluno.email" class="form-control"
											placeholder="Email" required>
									</div>
									<div ng-show="fRegister.$submitted || fRegister.email.$touched">
										<div ng-show="fRegister.email.$error.required">
											<p class="wrong">O email Ã© obrigatÃ³rio.</p>
										</div>
										<div ng-show="fRegister.email.$error.email">
											<p class="wrong">O campo email deverÃ¡ conter um email.</p>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="col-xs-12 input-group">
										<span class="input-group-addon"> <i
											class="glyphicon glyphicon-home"></i>
										</span> <input id="idCidade" name="cidade"
											ng-model="vm.usuarioAluno.cidade" type="Text"
											class="form-control" placeholder="CEP" required>
									</div>
									<div
										ng-show="fRegister.$submitted || fRegister.cidade.$touched">
										<div ng-show="fRegister.cidade.$error.required">
											<p class="wrong">O CEP é obrigatório!.</p>
										</div>
									</div>
								</div>
<!-- 
								<div class="form-group">
									<div class="col-xs-12 input-group">
										<span class="input-group-addon"> <i
											class="glyphicon glyphicon-home"></i>
										</span> <input id="idBairro" name="bairro"
											ng-model="vm.usuarioAluno.bairro" type="Text"
											class="form-control" placeholder="Bairro" required>
									</div>
									<div
										ng-show="fRegister.$submitted || fRegister.bairro.$touched">
										<div ng-show="fRegister.bairro.$error.required">
											<p class="wrong">O bairro Ã© obrigatÃ³rio.</p>
										</div>
									</div>
								</div> -->

								<div class="form-group">
									<div class="col-xs-12 input-group">
										<span class="input-group-addon"> <i
											class="glyphicon glyphicon-lock"></i>
										</span> <input id="senha" name="senha" type="password"
											ng-model="vm.usuarioAluno.senha"
											ng-bind="uc.capsLockisOn('#fRegister')" class="form-control"
											placeholder="Senha" required>
									</div>
									<div ng-show="fRegister.$submitted || fRegister.senha.$touched">
										<div ng-show="fRegister.senha.$error.required">
											<p class="wrong">A senha Ã© obrigatÃ³ria.</p>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="col-xs-12 input-group">
										<span class="input-group-addon"> <i
											class="glyphicon glyphicon-lock"></i>
										</span> <input id="senhaConf" name="senhaConf" type="password"
											ng-model="vm.senhaConf"
											ng-bind="vm.confimacaoSenhaAluno(fRegister)"
											class="form-control" placeholder="Confirme sua senha"
											required>
									</div>
									<div
										ng-show="fRegister.$submitted || fRegister.senhaConf.$touched">
										<div ng-show="fRegister.senhaConf.$error.required">
											<p class="wrong">A confirmaÃ§Ã£o de senha Ã©
												obrigatÃ³ria.</p>
										</div>
										<div ng-show="fRegister.senhaConf.$error.senhaConfError">
											<p class="wrong">As senhas nÃ£o conferem.</p>
										</div>
									</div>
								</div>

								<div ng-if="vm.usuarioAluno.id_usuario == 0">
									<div class="form-group">
										<div class="col-xs-12 input-group">
											<label class="checkbox-inline pull-left"> <input
												type="checkbox" value="" ng-model="vm.continuarCadastro">Continuar
												cadastrando?
											</label>
										</div>
									</div>
								</div>

								<div ng-show="vm.showErrorAluno() != ''">
									<div class="text-center alert alert-danger">
										{{vm.showErrorAluno()}}</div>
								</div>

								<div ng-show="uc.control.bol">
									<div class="text-center alert alert-warning">
										{{uc.control.error}}</div>
								</div>

								<div class="form-group">
									<div class="col-xs-12 input-group">
										<div class="col-xs-6 pull-left">
											<div ng-if="vm.usuarioAluno.id_usuario == 0">
												<button name="alterar" type="submit" class="btn btn-primary"
													ng-disabled="fRegister.$invalid"
													ng-click="vm.saveUserAluno(vm.usuarioAluno, fRegister)">
													<span class="glyphicon glyphicon-floppy-save"></span>&nbsp;
													Cadastrar
												</button>
											</div>
											<div ng-if="vm.usuarioAluno.id_usuario != 0">
												<button name="alterar" type="submit" class="btn btn-primary"
													ng-disabled="fRegister.$invalid"
													ng-click="vm.updateUserAluno(vm.usuarioAluno, fRegister)">
													<span class="glyphicon glyphicon-floppy-save"></span>&nbsp;
													Alterar 
												</button>
											</div>
										</div>
										<div class="col-xs-6 pull-right">
											<button type="button" class="btn btn-danger"
												ng-click="vm.resetFormAluno(fRegister)" data-dismiss="modal">
												<span class="glyphicon glyphicon-remove"></span>&nbsp;
												Fechar
											</button>
										</div>
									</div>
								</div>

							</div>
						</div>
						<!-- <div class="col-lg-1 col-md-1 col-sm-2 col-xs-0"></div> -->
					</form>
					<!-- fim formulario -->

				</div>
			</div>
		</div>
	</div>
	<!-- fim modal -->


	<!-- cadastro responsavel Modal -->
	<div class="row">

		<div name="RegisterModal_responsavel" id="RegisterModal_responsavel"
			class="modal fade bd-modal-lg" tabindex="-1" role="dialog"
			aria-labelledby="RegisterModalLabel" aria-hidden="true"
			data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">

					<!-- formulario cadastro -->
					<form name="fRegister_responsavel" id="fRegister_responsavel"
						ng-controller="utilsController as uc">
						<div class="col-lg-1 col-md-1 col-sm-2 col-xs-0"></div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-12">

							<div class="text-center well">
								<h1 id="login">Cadastro ResponsÃ¡vel</h1>
								<hr />

								<div class="form-group">
									<div class="col-xs-12 input-group">
										<span class="input-group-addon"> <i
											class="glyphicon glyphicon-user"></i>
										</span> <input id="idNome" name="nome"
											ng-model="vm.usuarioResponsavel.nome" type="Text"
											class="form-control" placeholder="Nome completo" required>
									</div>
									<div
										ng-show="fRegister_responsavel.$submitted || fRegister_responsavel.nome.$touched">
										<div ng-show="fRegister_responsavel.nome.$error.required">
											<p class="wrong">O nome Ã© obrigatÃ³rio.</p>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="col-xs-12 input-group">
										<span class="input-group-addon"> <i
											class="glyphicon glyphicon-envelope"></i>
										</span> <input id="idEmail" name="email" type="email"
											ng-model="vm.usuarioResponsavel.email" class="form-control"
											placeholder="Email" required>
									</div>
									<div
										ng-show="fRegister_responsavel.$submitted || fRegister_responsavel.email.$touched">
										<div ng-show="fRegister_responsavel.email.$error.required">
											<p class="wrong">O email Ã© obrigatÃ³rio.</p>
										</div>
										<div ng-show="fRegister_responsavel.email.$error.email">
											<p class="wrong">O campo email deverÃ¡ conter um email.</p>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="col-xs-12 input-group">
										<span class="input-group-addon"> <i
											class="glyphicon glyphicon-user"></i>
										</span> <input id="idCPF" name="cpf"
											ng-model="vm.usuarioResponsavel.cpf" type="Text"
											class="form-control" placeholder="CPF" required>
									</div>
									<div
										ng-show="fRegister_responsavel.$submitted || fRegister_responsavel.cpf.$touched">
										<div ng-show="fRegister_responsavel.cpf.$error.required">
											<p class="wrong">O CPF Ã© obrigatÃ³rio.</p>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="col-xs-12 input-group">
										<span class="input-group-addon"> <i
											class="glyphicon glyphicon-lock"></i>
										</span> <input id="senha" name="senha" type="password"
											ng-model="vm.usuarioResponsavel.senha"
											ng-bind="uc.capsLockisOn('#fRegister_responsavel')"
											class="form-control" placeholder="Senha" required>
									</div>
									<div
										ng-show="fRegister_responsavel.$submitted || fRegister_responsavel.senha.$touched">
										<div ng-show="fRegister_responsavel.senha.$error.required">
											<p class="wrong">A senha Ã© obrigatÃ³ria.</p>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="col-xs-12 input-group">
										<span class="input-group-addon"> <i
											class="glyphicon glyphicon-lock"></i>
										</span> <input id="senhaConf" name="senhaConf" type="password"
											ng-model="vm.senhaConf"
											ng-bind="vm.confimacaoSenhaResponsavel(fRegister_responsavel)"
											class="form-control" placeholder="Confirme sua senha"
											required>
									</div>
									<div
										ng-show="fRegister_responsavel.$submitted || fRegister_responsavel.senhaConf.$touched">
										<div ng-show="fRegister_responsavel.senhaConf.$error.required">
											<p class="wrong">A confirmaÃ§Ã£o de senha Ã©
												obrigatÃ³ria.</p>
										</div>
										<div
											ng-show="fRegister_responsavel.senhaConf.$error.senhaConfError">
											<p class="wrong">As senhas nÃ£o conferem.</p>
										</div>
									</div>
								</div>

								<div ng-if="vm.usuarioResponsavel.id_usuario == 0">
									<div class="form-group">
										<div class="col-xs-12 input-group">
											<label class="checkbox-inline pull-left"> <input
												type="checkbox" value="" ng-model="vm.continuarCadastro">Continuar
												cadastrando?
											</label>
										</div>
									</div>
								</div>

								<div ng-show="vm.showErrorResponsavel() != ''">
									<div class="text-center alert alert-danger">
										{{vm.showErrorResponsavel()}}</div>
								</div>

								<div ng-show="uc.control.bol">
									<div class="text-center alert alert-warning">
										{{uc.control.error}}</div>
								</div>

								<div class="form-group">
									<div class="col-xs-12 input-group">
										<div class="col-xs-6 pull-left">
											<div ng-if="vm.usuarioResponsavel.id_usuario == 0">
												<button name="alterar" type="submit" class="btn btn-primary"
													ng-disabled="fRegister_responsavel.$invalid"
													ng-click="vm.saveUserResponsavel(vm.usuarioResponsavel, fRegister_responsavel)">
													<span class="glyphicon glyphicon-floppy-save"></span>&nbsp;
													Cadastrar
												</button>
											</div>
											<div ng-if="vm.usuarioResponsavel.id_usuario != 0">
												<button name="alterar" type="submit" class="btn btn-primary"
													ng-disabled="fRegister_responsavel.$invalid"
													ng-click="vm.updateUserResponsavel(vm.usuarioResponsavel, fRegister_responsavel)">
													<span class="glyphicon glyphicon-floppy-save"></span>&nbsp;
													Alterar
												</button>
											</div>
											
											
										</div>
										<div class="col-xs-6 pull-right">
											<button type="button" class="btn btn-danger"
												ng-click="vm.resetFormResponsavel(fRegister_responsavel)"
												data-dismiss="modal">
												<span class="glyphicon glyphicon-remove"></span>&nbsp;
												Fechar
											</button>
										</div>
									</div>
								</div>


							</div>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-2 col-xs-0"></div>
					</form>
					<!-- fim formulario -->

				</div>
			</div>
		</div>
	</div>
	<!-- fim modal -->

	<!-- cadastro professor Modal -->
	<div class="row">

		<div name="RegisterModal_professor" id="RegisterModal_professor"
			class="modal fade bd-modal-lg" tabindex="-1" role="dialog"
			aria-labelledby="RegisterModalLabel" aria-hidden="true"
			data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">

					<!-- formulario cadastro -->
					<form name="fRegister_professor" id="fRegister_professor"
						ng-controller="utilsController as uc">
						<div class="col-lg-1 col-md-1 col-sm-2 col-xs-0"></div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-12">

							<div class="text-center well">
								<h1 id="login">Cadastro Professor</h1>
								<hr />

								<div class="form-group">
									<div class="col-xs-12 input-group">
										<span class="input-group-addon"> <i
											class="glyphicon glyphicon-user"></i>
										</span> <input id="idNome" name="nome"
											ng-model="vm.usuarioProfessor.nome" type="Text"
											class="form-control" placeholder="Nome completo" required>
									</div>
									<div
										ng-show="fRegister_professor.$submitted || fRegister_professor.nome.$touched">
										<div ng-show="fRegister_professor.nome.$error.required">
											<p class="wrong">o nome é obrigatório.</p>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="col-xs-12 input-group">
										<span class="input-group-addon"> <i
											class="glyphicon glyphicon-envelope"></i>
										</span> <input id="idEmail" name="email" type="email"
											ng-model="vm.usuarioProfessor.email" class="form-control"
											placeholder="Email" required>
									</div>
									<div
										ng-show="fRegister_professor.$submitted || fRegister_professor.email.$touched">
										<div ng-show="fRegister_professor.email.$error.required">
											<p class="wrong">O email é obrigatório.</p>
										</div>
										<div ng-show="fRegister_professor.email.$error.email">
											<p class="wrong">O campo email deverá conter um email.</p>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="col-xs-12 input-group">
										<span class="input-group-addon"> <i
											class="glyphicon glyphicon-user"></i>
										</span> <input id="idCPF" name="cpf"
											ng-model="vm.usuarioProfessor.cpf" type="Text"
											class="form-control" placeholder="CPF" required>
									</div>
									<div
										ng-show="fRegister_professor.$submitted || fRegister_professor.cpf.$touched">
										<div ng-show="fRegister_professor.cpf.$error.required">
											<p class="wrong">O CPF Ã© obrigatÃ³rio.</p>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="col-xs-12 input-group">
										<span class="input-group-addon"> <i
											class="glyphicon glyphicon-lock"></i>
										</span> <input id="senha" name="senha" type="password"
											ng-model="vm.usuarioProfessor.senha"
											ng-bind="uc.capsLockisOn('#fRegister_professor')"
											class="form-control" placeholder="Senha" required>
									</div>
									<div
										ng-show="fRegister_professor.$submitted || fRegister_professor.senha.$touched">
										<div ng-show="fRegister_professor.senha.$error.required">
											<p class="wrong">A senha Ã© obrigatÃ³ria.</p>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="col-xs-12 input-group">
										<span class="input-group-addon"> <i
											class="glyphicon glyphicon-lock"></i>
										</span> <input id="senhaConf" name="senhaConf" type="password"
											ng-model="vm.senhaConf"
											ng-bind="vm.confimacaoSenhaProfessor(fRegister_professor)"
											class="form-control" placeholder="Confirme sua senha"
											required>
									</div>
									<div
										ng-show="fRegister_professor.$submitted || fRegister_professor.senhaConf.$touched">
										<div ng-show="fRegister_professor.senhaConf.$error.required">
											<p class="wrong">A confirmaÃ§Ã£o de senha Ã©
												obrigatÃ³ria.</p>
										</div>
										<div
											ng-show="fRegister_professor.senhaConf.$error.senhaConfError">
											<p class="wrong">As senhas nÃ£o conferem.</p>
										</div>
									</div>
								</div>

								<div ng-if="vm.usuarioProfessor.id_usuario == 0">
									<div class="form-group">
										<div class="col-xs-12 input-group">
											<label class="checkbox-inline pull-left"> <input
												type="checkbox" value="" ng-model="vm.continuarCadastro">Continuar
												cadastrando?
											</label>
										</div>
									</div>
								</div>

								<div ng-show="vm.showErrorProfessor() != ''">
									<div class="text-center alert alert-danger">
										{{vm.showErrorProfessor()}}</div>
								</div>

								<div ng-show="uc.control.bol">
									<div class="text-center alert alert-warning">
										{{uc.control.error}}</div>
								</div>

								<div class="form-group">
									<div class="col-xs-12 input-group">
										<div class="col-xs-6 pull-left">
											<div ng-if="vm.usuarioProfessor.id_usuario == 0">
												<button name="alterar" type="submit" class="btn btn-primary"
													ng-disabled="fRegister_professor.$invalid"
													ng-click="vm.saveUserProfessor(vm.usuarioProfessor, fRegister_professor)">
													<span class="glyphicon glyphicon-floppy-save"></span>&nbsp;
													Cadastrar
												</button>
											</div>
											<div ng-if="vm.usuarioProfessor.id_usuario != 0">
												<button name="alterar" type="submit" class="btn btn-primary"
													ng-disabled="fRegister_professor.$invalid"
													ng-click="vm.updateUserProfessor(vm.usuarioProfessor, fRegister_professor)">
													<span class="glyphicon glyphicon-floppy-save"></span>&nbsp;
													Alterar
												</button>
											</div>
										</div>
										<div class="col-xs-6 pull-right">
											<button type="button" class="btn btn-danger"
												ng-click="vm.resetFormProfessor(fRegister_professor)"
												data-dismiss="modal">
												<span class="glyphicon glyphicon-remove"></span>&nbsp;
												Fechar
											</button>
										</div>
									</div>
								</div>

							</div>
						</div>
						<div class="col-lg-1 col-md-1 col-sm-2 col-xs-0"></div>
					</form>
					<!-- fim formulario -->

				</div>
			</div>
		</div>
		<!-- fim modal -->

	</div>
</div>



<script
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>



<script
	src="https://rawgithub.com/eligrey/FileSaver.js/master/FileSaver.js"
	type="text/javascript"></script>

<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.debug.js"></script>

<script src="../protected/controller/usuariosController.js"></script>

<script src="../protected/service/usuariosService.js"></script>

<script src="../resources/js/utilsController.js"></script>