<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>



<div class="col-sm-12" ng-init="vm.selectedTab = 'prop'"
	ng-controller="propagandaController as vm">
	<div class="row">
		<div class="col-sm-12 text-center spacing">
			<h1>Consultar Propagandas</h1>
		</div>
	</div>


	<div class="row">
		<div class="col-sm-4">
			<button type="button" class="btn btn-primary" data-toggle="modal"
				data-target="#RegisterModal_propaganda">
				<span class="glyphicon glyphicon-plus"></span>&nbsp;Adicionar
				Propaganda
			</button>
		</div>
		<div class="col-sm-offset-4 col-sm-4 spacing">
			<div class="inner-addon right-addon">
				<i class="glyphicon glyphicon-search"></i> <input type="text"
					class="form-control" placeholder="Procurar Propagandas"
					ng-model="procurar_p" />
			</div>
		</div>
	</div>

	<!-- cadastro propaganda Modal -->
	<div class="row">

		<div name="RegisterModal_propaganda" id="RegisterModal_propaganda"
			class="modal fade bd-modal-lg" tabindex="-1" role="dialog"
			aria-labelledby="RegisterModalLabel" aria-hidden="true"
			data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">

					<!-- formulario cadastro -->
					<form name="fRegister" id="fRegister"
						ng-controller="utilsController as uc">
						<!-- <div class="col-lg-1 col-md-1 col-sm-2 col-xs-0"></div> -->
						<div class="col-xs-12">

							<div class="text-center well">
								<h2 id="login">Cadastro de novas propagandas</h2>
								<hr />

								<div class="form-group">
									<div class="col-xs-12 input-group">
										<span class="input-group-addon"> <i
											class="glyphicon glyphicon-book"></i>
										</span> <input id="idNome" name="nome" ng-model="vm.propaganda.nome"
											type="Text" class="form-control"
											placeholder="Nome da propaganda completo" required>
									</div>
									<div ng-show="fRegister.$submitted || fRegister.nome.$touched">
										<div ng-show="fRegister.nome.$error.required">
											<p class="wrong">O nome é Obrigatório</p>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="col-xs-12 input-group">
										<span class="input-group-addon"> <i
											class="glyphicon glyphicon-comment"></i>
										</span> <input id="idUrl" name="url"
											ng-model="vm.propaganda.imagem_url" type="text"
											class="form-control"
											placeholder="Insira a URL da imagem desejada:" required>
									</div>
									<div ng-show="fRegister.$submitted || fRegister.url.$touched">
										<div ng-show="fRegister.url.$error.required">
											<p class="wrong">A URL é obrigatória!</p>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="col-xs-12 input-group">
										<span class="input-group-addon"> <i
											class="glyphicon glyphicon-calendar"></i>
										</span> <input id="idData" name="data" ng-model="vm.propaganda.data"
											type="Date" class="form-control" required>
									</div>
									<div ng-show="fRegister.$submitted || fRegister.data.$touched">
										<div ng-show="fRegister.data.$error.required">
											<p class="wrong">A data é obrigatória.</p>
										</div>
									</div>
								</div>



								<div ng-if="vm.propaganda.id_propaganda == 0">
									<div class="form-group">
										<div class="col-xs-12 input-group">
											<label class="checkbox-inline pull-left"> <input
												type="checkbox" value="" ng-model="vm.continuarCadastro">Continuar
												cadastrando?
											</label>
										</div>
									</div>
								</div>

								<div ng-show="vm.showError() != ''">
									<div class="text-center alert alert-danger">
										{{vm.showError()}}</div>
								</div>

								<div ng-show="uc.control.bol">
									<div class="text-center alert alert-warning">
										{{uc.control.error}}</div>
								</div>

								<div class="form-group">
									<div class="col-xs-12 input-group">
										<div class="col-xs-6 pull-left">
											<div ng-if="vm.propaganda.id_propaganda  == 0">
												<button name="alterar" type="submit" class="btn btn-primary"
													ng-disabled="fRegister.$invalid"
													ng-click="vm.savePropaganda(vm.propaganda, fRegister)">
													<span class="glyphicon glyphicon-floppy-save"></span>&nbsp;
													Cadastrar
												</button>
											</div>
											<div ng-if="vm.propaganda.id_propaganda != 0">
												<button name="alterar" type="submit" class="btn btn-primary"
													ng-disabled="fRegister.$invalid"
													ng-click="vm.updatePropaganda(vm.propaganda, fRegister)">
													<span class="glyphicon glyphicon-floppy-save"></span>&nbsp;
													Alterar
												</button>
											</div>
										</div>
										<div class="col-xs-6 pull-right">
											<button type="button" class="btn btn-danger"
												ng-click="vm.resetForm(fRegister)" data-dismiss="modal">
												<span class="glyphicon glyphicon-remove"></span>&nbsp;
												Fechar
											</button>
										</div>
									</div>
								</div>

							</div>
						</div>
						<!-- <div class="col-lg-1 col-md-1 col-sm-2 col-xs-0"></div> -->
					</form>
					<!-- fim formulario -->

				</div>
			</div>
		</div>
	</div>
	<!-- fim modal -->

	<div class="col-md-3 " ng-repeat="propaganda in vm.propagandaList | filter:propaganda.nome = procurar_p">
		<div class="propaganda_box">
			<div class="row">
				<img class="center-block img-fluid d-block mx-auto"
					src={{propaganda.imagem_url}}
					width="300" height="300">
			</div>

			<div class=" h3 row text-center propagandas_space">{{propaganda.nome}}</div>
			<div class="h4 row text-center propagandas_space">{{propaganda.data | date : "dd/MM/yyyy"}}</div>
			<div class="row text-center" style="padding-bottom: 10px;">

				<input type="button" name="Editar" value="Editar"
					ng-click="vm.exibirDados(propaganda)"
					data-toggle="modal" data-target="#RegisterModal_propaganda"
					class="btn btn-success btn-sn"> <input type="button"
					ng-click="vm.deletePropaganda(propaganda.id_propaganda, propaganda)"
					name="Excluir" value="Excluir" class="btn btn-danger btn-sn">
			</div>

		</div>
	</div>


</div>

<script src="../protected/controller/propagandaController.js"> </script>
<script src="../protected/service/propagandaService.js"> </script>
<script src="../resources/js/utilsController.js"> </script>

