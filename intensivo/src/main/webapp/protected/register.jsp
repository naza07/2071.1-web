<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="col-xs-12" ng-controller="cadastroController as vm">
	<div class="row-fluid">

		<h1 id="tab_title">Usuários Cadastrados</h1>
		<table class="table table-hover">
			<thead>
				<tr>
					<th class="col-xs-5">Nome completo</th>
					<th class="col-xs-5">Email</th>
					<th class="col-xs-2">Ações</th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="usuario in vm.usuarioList | orderBy: 'tipoUsuarioIdTipoUsuario'">
					<td class="col-xs-5">{{usuario.nome}}</td>
					<td class="col-xs-5">{{usuario.email}}</td>
					<td class="col-xs-2">
						<button type="submit" class="btn btn-success btn-sm"
							data-toggle="modal" data-target="#RegisterModal"
							ng-click="vm.exibirDados(usuario)">
							<span class="glyphicon glyphicon-pencil"></span>&nbsp; Alterar
						</button>
						<button type="button" class="btn btn-danger btn-sm"
							ng-click="vm.deleteUser(usuario.idavaliacaousuario, usuario)">
							<span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;
							Excluir
						</button>
					</td>
				</tr>
			</tbody>
		</table>

		<!-- cadastro usuario Modal -->
		<div class="row-fluid">

			<div name="RegisterModal" id="RegisterModal"
				class="modal fade bd-modal-lg" tabindex="-1" role="dialog"
				aria-labelledby="RegisterModalLabel" aria-hidden="true"
				data-backdrop="static" data-keyboard="false">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">

						<!-- formulario cadastro -->
						<form name="fRegister" id="fRegister"
							ng-controller="utilsController as uc">
							<div class="col-lg-1 col-md-1 col-sm-2 col-xs-0"></div>
							<div class="col-lg-10 col-md-10 col-sm-8 col-xs-12">

								<div class="text-center well">
									<h1 id="login">Cadastro Usuário</h1>
									<hr />

									<div class="form-group">
										<div class="col-xs-12 input-group">
											<span class="input-group-addon"> <i
												class="glyphicon glyphicon-user"></i>
											</span> <input id="idNome" name="nome" ng-model="vm.usuario.nome"
												type="Text" class="form-control" placeholder="Nome completo"
												required>
										</div>
										<div ng-show="fRegister.$submitted || fRegister.nome.$touched">
											<div ng-show="fRegister.nome.$error.required">
												<p class="wrong">O nome é obrigatório.</p>
											</div>
										</div>
									</div>

									<div class="form-group">
										<div class="col-xs-12 input-group">
											<span class="input-group-addon"> <i
												class="glyphicon glyphicon-envelope"></i>
											</span> <input id="idEmail" name="email" type="email"
												ng-model="vm.usuario.email" class="form-control"
												placeholder="Email" required>
										</div>
										<div
											ng-show="fRegister.$submitted || fRegister.email.$touched">
											<div ng-show="fRegister.email.$error.required">
												<p class="wrong">O email é obrigatório.</p>
											</div>
											<div ng-show="fRegister.email.$error.email">
												<p class="wrong">O campo email deverá conter um email.</p>
											</div>
										</div>
									</div>

									<div class="form-group">
										<div class="col-xs-12 input-group">
											<span class="input-group-addon"> <i
												class="glyphicon glyphicon-lock"></i>
											</span> <input id="senha" name="senha" type="password"
												ng-model="vm.usuario.senha"
												ng-bind="uc.capsLockisOn('#fRegister')" class="form-control"
												placeholder="Senha" required>
										</div>
										<div
											ng-show="fRegister.$submitted || fRegister.senha.$touched">
											<div ng-show="fRegister.senha.$error.required">
												<p class="wrong">A senha é obrigatória.</p>
											</div>
										</div>
									</div>

									<div class="form-group">
										<div class="col-xs-12 input-group">
											<span class="input-group-addon"> <i
												class="glyphicon glyphicon-lock"></i>
											</span> <input id="senhaConf" name="senhaConf" type="password"
												ng-model="vm.senhaConf"
												ng-bind="vm.confimacaoSenha(fRegister)" class="form-control"
												placeholder="Confirme sua senha" required>
										</div>
										<div
											ng-show="fRegister.$submitted || fRegister.senhaConf.$touched">
											<div ng-show="fRegister.senhaConf.$error.required">
												<p class="wrong">A confirmação de senha é
													obrigatória.</p>
											</div>
											<div ng-show="fRegister.senhaConf.$error.senhaConfError">
												<p class="wrong">As senhas não conferem.</p>
											</div>
										</div>
									</div>

									<div ng-if="vm.usuario.idavaliacaousuario == 0">
										<div class="form-group">
											<div class="col-xs-12 input-group">
												<label class="checkbox-inline pull-left"> <input
													type="checkbox" value="" ng-model="vm.continuarCadastro">Continuar
													cadastrando?
												</label>
											</div>
										</div>
									</div>

									<div ng-show="vm.showError() != ''">
										<div class="text-center alert alert-danger">
											{{vm.showError()}}</div>
									</div>

									<div ng-show="uc.control.bol">
										<div class="text-center alert alert-warning">
											{{uc.control.error}}</div>
									</div>

									<div class="form-group">
										<div class="col-xs-12 input-group">
											<div class="col-xs-6 pull-left">
												<div ng-if="vm.usuario.idavaliacaousuario == 0">
													<button name="alterar" type="submit"
														class="btn btn-primary" ng-disabled="fRegister.$invalid"
														ng-click="vm.saveUser(vm.usuario, fRegister)">
														<span class="glyphicon glyphicon-floppy-save"></span>&nbsp;
														Cadastrar
													</button>
												</div>
												<div ng-if="vm.usuario.idavaliacaousuario != 0">
													<button name="alterar" type="submit"
														class="btn btn-primary" ng-disabled="fRegister.$invalid"
														ng-click="vm.updateUser(vm.usuario, fRegister)">
														<span class="glyphicon glyphicon-floppy-save"></span>&nbsp;
														Alterar
													</button>
												</div>
											</div>
											<div class="col-xs-6 pull-right">
												<button type="button" class="btn btn-danger"
													ng-click="vm.resetForm(fRegister)" data-dismiss="modal">
													<span class="glyphicon glyphicon-remove"></span>&nbsp;
													Fechar
												</button>
											</div>
										</div>
									</div>

								</div>
							</div>
							<div class="col-lg-1 col-md-1 col-sm-2 col-xs-0"></div>
						</form>
						<!-- fim formulario -->

					</div>
				</div>
			</div>
			<!-- fim modal -->

			<div class="row-fluid text-center">
				<button class="btn btn-primary" data-toggle="modal"
					data-target="#RegisterModal">
					<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;
					Cadastrar novo usuário
				</button>
				<br /> <br />
			</div>
		</div>
	</div>
</div>

<script src="../protected/controller/cadastroController.js"></script>
<script src="../protected/service/cadastroService.js"></script>
<script src="../resources/js/utilsController.js"></script>
