package br.ufrpe.intensivo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.ufrpe.intensivo.model.Notificacao;
import br.ufrpe.intensivo.repository.NotificacaoRepository;
import br.ufrpe.intensivo.repository.UsuarioRepository;

@Service
@Transactional
public class NotificacaoService {

	@Autowired
	private NotificacaoRepository notificacaoRepository;

	@Autowired
	private MessageSource messageSource;

	public NotificacaoService() {
	}

	public NotificacaoService(NotificacaoRepository notificacaoRepository, MessageSource messageSource) {
		this.notificacaoRepository = notificacaoRepository;
		this.messageSource = messageSource;
	}

	public void saveNotificacao(Notificacao notificacao) {
		notificacaoRepository.save(notificacao);
	}

	public Notificacao findById(Integer idNotificacao) {
		return notificacaoRepository.findByIdNotificacao(idNotificacao);
	}

	public List<Notificacao> findAllValidNotificacoes() {
		return notificacaoRepository.findAllValidNotificacoes();
	}

}
