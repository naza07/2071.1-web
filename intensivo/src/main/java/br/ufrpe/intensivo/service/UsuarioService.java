package br.ufrpe.intensivo.service;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.*;
import br.ufrpe.intensivo.builder.UsuarioBuilder;
import br.ufrpe.intensivo.exception.NegocioException;
import br.ufrpe.intensivo.model.Usuario;
import br.ufrpe.intensivo.repository.UsuarioRepository;
import br.ufrpe.intensivo.util.Util;
import br.ufrpe.intensivo.vo.Endereco;

@Service
@Transactional
public class UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private MessageSource messageSource;

	public UsuarioService() {
	}

	public UsuarioService(UsuarioRepository usuarioRepository, MessageSource messageSource) {
		this.usuarioRepository = usuarioRepository;
		this.messageSource = messageSource;
	}

	public Usuario verificarLogin(String email, String senha) throws NegocioException {
		Usuario intensivousuario = usuarioRepository.findByEmail(email);
		String senhaCriptografada;

		if (intensivousuario == null) {
			throw new NegocioException("erro.login.2", messageSource);
		}

		if (intensivousuario.getTipoUsuarioIdTipoUsuario() != 2) {
			throw new NegocioException("erro.login.3", messageSource);
		}

		try {
			senhaCriptografada = Util.criptografarMD5(senha);
		} catch (NoSuchAlgorithmException e) {
			throw new NegocioException("erro.login.1", messageSource);
		}

		if (senhaCriptografada.equals(intensivousuario.getSenha())) {
			return intensivousuario;
		} else {
			throw new NegocioException("erro.login.1", messageSource);
		}

	}

	public Usuario salvarUsuarioWeb(Usuario usuario) throws NegocioException {

		Usuario usuarioLogin = usuarioRepository.findByLogin(usuario.getLogin());

		if (usuarioLogin != null) {
			throw new NegocioException("erro.cadastro.1", messageSource);
		}

		if (usuario.getTipoUsuarioIdTipoUsuario() == 1) {
			try {
				String url = "https://viacep.com.br/ws/" + usuario.getCidade() + "/json/";
				RestTemplate restTemplate = new RestTemplate();
				Endereco endereco = restTemplate.getForObject(url, Endereco.class, 200);
				usuario.setCidade(endereco.getLocalidade());
				usuario.setBairro(endereco.getLogradouro());

			} catch (Exception e) {
				throw new NegocioException("erro.cadastro.4", messageSource);
			}
		}

		this.validarSenha(usuario);
		try {
			usuario.setSenha(Util.criptografarMD5(usuario.getSenha()));
			return usuarioRepository.save(usuario);
		} catch (NoSuchAlgorithmException e1) {
			throw new NegocioException("erro.cadastro.3", messageSource);
		}

	}

	public Usuario salvarAvaliacaoUsuario(Usuario usuario) throws NegocioException {
		Usuario avaliacaoUsuarioEmail = usuarioRepository.findByEmail(usuario.getEmail());

		if (avaliacaoUsuarioEmail != null) {
			throw new NegocioException("erro.cadastro.1", messageSource);
		}
		this.validarSenha(usuario);

		try {
			return usuarioRepository
					.save(new UsuarioBuilder(usuario).senha(Util.criptografarMD5(usuario.getSenha())).build());
		} catch (NoSuchAlgorithmException e) {
			throw new NegocioException("erro.cadastro.3", messageSource);
		}
	}

	public Usuario alterarUsuario(Usuario usuario) throws NegocioException {
		Usuario usuarioUpdate = this.findOne(usuario.getId_usuario());

		// Usuario usuarioEmail = usuarioRepository.findByEmail(usuario.getEmail());

		if (usuarioUpdate == null) {
			throw new NegocioException("erro.cadastro.1", messageSource);
		}
		this.validarSenha(usuario);

		try {
			usuario.setSenha(Util.criptografarMD5(usuario.getSenha()));
			return usuarioRepository.save(usuario);
		} catch (NoSuchAlgorithmException e1) {
			throw new NegocioException("erro.cadastro.3", messageSource);
		}

	}

	public Usuario deletarUsuario(Integer id) throws NegocioException {
		Usuario usuario = this.findOne(id);
		usuario.setIsAtivo(false);
		return usuarioRepository.save(usuario);
	}

	public List<Usuario> consultarListaUsuarios() {
		return usuarioRepository.findAllUsuarios();
	}

	private Usuario findOne(Integer id) throws NegocioException {
		Usuario usuario = usuarioRepository.findOne(id);

		if (usuario != null) {
			return usuario;
		} else {
			throw new NegocioException("erro.cadastro.4", messageSource);
		}
	}

	private void validarSenha(Usuario usuario) throws NegocioException {
		if (usuario.getSenha().length() <= 6) {
			throw new NegocioException("erro.cadastro.2", messageSource);
		}
		/*
		 * if (!usuario.getSenha().matches("(([a-z]+[0-9]+)|([0-9]+[a-z]+))+")) { throw
		 * new NegocioException("erro.cadastro.2", messageSource); }
		 */
	}
}
