package br.ufrpe.intensivo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import br.ufrpe.intensivo.model.Propaganda;
import br.ufrpe.intensivo.repository.PropagandaRepository;

@Service
@Transactional
public class PropagandaService {
	
	@Autowired
	private PropagandaRepository propagandaRepository;

	@Autowired
	private MessageSource messageSource;

	public PropagandaService() {
	}

	public PropagandaService(PropagandaRepository propagandaRepository, MessageSource messageSource) {
		this.propagandaRepository = propagandaRepository;
		this.messageSource = messageSource;
	}

	public void savePropaganda(Propaganda propaganda) {
		propagandaRepository.save(propaganda);
	}

	public Propaganda findById(Integer idPropaganda) {
		return propagandaRepository.findByIdNotificacao(idPropaganda);
	}

	public List<Propaganda> findAllValidPropagandas() {
		return propagandaRepository.findAllValidPropagandas();
		
	}

}
