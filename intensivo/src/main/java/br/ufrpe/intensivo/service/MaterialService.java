package br.ufrpe.intensivo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.ufrpe.intensivo.model.Material;
import br.ufrpe.intensivo.repository.MaterialRepository;


@Service
@Transactional
public class MaterialService {
	
	@Autowired
	private MaterialRepository materialRepository;

	@Autowired
	private MessageSource messageSource;

	public MaterialService() {
	}

	public MaterialService(MaterialRepository materialRepository, MessageSource messageSource) {
		this.materialRepository = materialRepository;
		this.messageSource = messageSource;
	}

	public void saveMaterial(Material material) {
		materialRepository.save(material);
	}

	public Material findById(Integer idMaterial) {
		return materialRepository.findByIdMaterial(idMaterial);
	}

	public List<Material> findAllValidMaterial() {
		return materialRepository.findAllValidMaterial();
	}

}
