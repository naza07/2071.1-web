package br.ufrpe.intensivo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.ufrpe.intensivo.model.Horario;
import br.ufrpe.intensivo.repository.HorariosRepository;

@Service
@Transactional
public class HorariosService {
	
	@Autowired
	private HorariosRepository horarioRepository;

	@Autowired
	private MessageSource messageSource;

	public HorariosService() {
	}

	public HorariosService(HorariosRepository horarioRepository, MessageSource messageSource) {
		this.horarioRepository = horarioRepository;
		this.messageSource = messageSource;
	}

	public void saveHorario(Horario horario) {
		horarioRepository.save(horario);		
	}

	public Horario findById(Integer idHorario) {
		return horarioRepository.findByIdHorario(idHorario);
	}

	public List<Horario> findAllValidHorario() {
		return horarioRepository.findAllValidHorario();
	}

}
