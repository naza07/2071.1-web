package br.ufrpe.intensivo.builder;

import java.util.Date;

import br.ufrpe.intensivo.model.Usuario;

public class UsuarioBuilder {
	private Usuario usuario;

	public UsuarioBuilder() {
		this.usuario = new Usuario();
	}
	
	public UsuarioBuilder(Usuario usuario) {
		this.usuario = usuario;
	}

	public UsuarioBuilder id (Integer id) {
		usuario.setId_usuario(id);
		return this;
	}

	public UsuarioBuilder email (String email) {
		usuario.setEmail (email);
		return this;
	}

	public UsuarioBuilder nome (String nome) {
		usuario.setNome(nome);
		return this;
	}

	public UsuarioBuilder senha (String senha) {
		usuario.setSenha(senha);
		return this;
	}

	/*public AvaliacaoUsuarioBuilder dataCadastro (Date data) {
		avaliacaoUsuario.setDatacadastro (data);
		return this;
	}

	public AvaliacaoUsuarioBuilder dataInativacao (Date data) {
		avaliacaoUsuario.setDatainativacao (data);
		return this;
	}

	public AvaliacaoUsuarioBuilder ativo (Boolean ativo) {
		avaliacaoUsuario.setAtivo (ativo);
		return this;
	}*/

	public Usuario build () {
		return usuario;
	}
}
