package br.ufrpe.intensivo.vo;

import java.util.Date;

public class NotificacaoVO {
	
	
	private Integer idnotificacao;
	private String titulo;
	private String descricao;
	private Date data;
	public Integer getIdnotificacao() {
		return idnotificacao;
	}
	public void setIdnotificacao(Integer idnotificacao) {
		this.idnotificacao = idnotificacao;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	
	

}
