package br.ufrpe.intensivo.vo;

import java.util.Date;

public class UsuarioVO {
	private Integer id_usuario;
	private String email;
	private String senha;
	private String nome;

	private Date dataNascimento;
	private String escolaQueEstuda;
	private String escolaridade;
	private String cpf;
	private String token;
	private String login;
	private Long usuarioIdUsuario;
	private Long tipoUsuarioIdTipoUsuario;
	private String bairro;
	private String cidade;
	private Boolean adiplenteOrInadiplente;
	private Boolean isAtivo;
	private String turma1;
	private String turma2;
	private String turma3;
	private String turma4;

	public Integer getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(Integer id_usuario) {
		this.id_usuario = id_usuario;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getEscolaQueEstuda() {
		return escolaQueEstuda;
	}

	public void setEscolaQueEstuda(String escolaQueEstuda) {
		this.escolaQueEstuda = escolaQueEstuda;
	}

	public String getEscolaridade() {
		return escolaridade;
	}

	public void setEscolaridade(String escolaridade) {
		this.escolaridade = escolaridade;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public Long getUsuarioIdUsuario() {
		return usuarioIdUsuario;
	}

	public void setUsuarioIdUsuario(Long usuarioIdUsuario) {
		this.usuarioIdUsuario = usuarioIdUsuario;
	}

	public Long getTipoUsuarioIdTipoUsuario() {
		return tipoUsuarioIdTipoUsuario;
	}

	public void setTipoUsuarioIdTipoUsuario(Long tipoUsuarioIdTipoUsuario) {
		this.tipoUsuarioIdTipoUsuario = tipoUsuarioIdTipoUsuario;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public Boolean getAdiplenteOrInadiplente() {
		return adiplenteOrInadiplente;
	}

	public void setAdiplenteOrInadiplente(Boolean adiplenteOrInadiplente) {
		this.adiplenteOrInadiplente = adiplenteOrInadiplente;
	}

	public Boolean getIsAtivo() {
		return isAtivo;
	}

	public void setIsAtivo(Boolean isAtivo) {
		this.isAtivo = isAtivo;
	}

	public String getTurma1() {
		return turma1;
	}

	public void setTurma1(String turma1) {
		this.turma1 = turma1;
	}

	public String getTurma2() {
		return turma2;
	}

	public void setTurma2(String turma2) {
		this.turma2 = turma2;
	}

	public String getTurma3() {
		return turma3;
	}

	public void setTurma3(String turma3) {
		this.turma3 = turma3;
	}

	public String getTurma4() {
		return turma4;
	}

	public void setTurma4(String turma4) {
		this.turma4 = turma4;
	}

}
