package br.ufrpe.intensivo.vo;

import java.sql.Time;

public class HorarioVO {
	
	private Integer idHorario;
	private Time inicio;
	private Time fim;
	private String dia;
	private Integer materia;
	private Boolean isAtivo;
	
	public Integer getIdHorario() {
		return idHorario;
	}
	public void setIdHorario(Integer idHorario) {
		this.idHorario = idHorario;
	}
	public Time getInicio() {
		return inicio;
	}
	public void setInicio(Time inicio) {
		this.inicio = inicio;
	}
	public Time getFim() {
		return fim;
	}
	public void setFim(Time fim) {
		this.fim = fim;
	}
	public String getDia() {
		return dia;
	}
	public void setDia(String dia) {
		this.dia = dia;
	}
	public Integer getMateria() {
		return materia;
	}
	public void setMateria(Integer materia) {
		this.materia = materia;
	}
	public Boolean getIsAtivo() {
		return isAtivo;
	}
	public void setIsAtivo(Boolean isAtivo) {
		this.isAtivo = isAtivo;
	}
	
}
