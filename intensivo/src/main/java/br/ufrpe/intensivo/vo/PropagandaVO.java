package br.ufrpe.intensivo.vo;

import java.util.Date;

public class PropagandaVO {
	private Integer id_propaganda;
	private String imagem_url;
	private String nome;
	private Date data;
	
	public Integer getId_propaganda() {
		return id_propaganda;
	}
	public void setId_propaganda(Integer id_propaganda) {
		this.id_propaganda = id_propaganda;
	}
	public String getImagem_url() {
		return imagem_url;
	}
	public void setImagem_url(String imagem_url) {
		this.imagem_url = imagem_url;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	
	

}
