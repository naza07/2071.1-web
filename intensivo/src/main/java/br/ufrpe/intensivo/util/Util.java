package br.ufrpe.intensivo.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;

import br.ufrpe.intensivo.model.Auditavel;

public class Util {

	public static String criptografarMD5(String senha) throws NoSuchAlgorithmException {
		MessageDigest md5 = MessageDigest.getInstance("MD5");
		md5.update (senha.getBytes());
		return DatatypeConverter.printHexBinary(md5.digest());
	}

	public static String montarJson (Auditavel objeto) {
		String json = "{";
		Map<String, Object> map = objeto.getValorObjeto();
		for (String key : map.keySet()) {
			json += key + ":" + map.get(key).toString() + ",";
		}
		json += "}";
		return json;
	}
}
