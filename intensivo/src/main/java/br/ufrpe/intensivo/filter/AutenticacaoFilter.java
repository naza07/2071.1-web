package br.ufrpe.intensivo.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import br.ufrpe.intensivo.exception.NegocioException;
import br.ufrpe.intensivo.model.Usuario;
import br.ufrpe.intensivo.service.UsuarioService;

public class AutenticacaoFilter extends UsernamePasswordAuthenticationFilter {

	private static Logger LOGGER = Logger.getLogger(AutenticacaoFilter.class.getName());

	@Autowired
	private UsuarioService usuarioService;

	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {

		String email = request.getParameter("j_username");
		String senha = request.getParameter("j_password");
		Usuario usuario = new Usuario();

		try {
			
			
			usuario = usuarioService.verificarLogin(email, senha);
			
		
			request.getSession().setAttribute("usuario", usuario);

			LOGGER.info("Usuário "+email+" está logado no sistema.");

			Collection<GrantedAuthority> regras = new ArrayList<GrantedAuthority>();
			return new UsernamePasswordAuthenticationToken(usuario.getEmail(), usuario.getSenha(), regras);

		} catch (NegocioException e) {
			throw new BadCredentialsException(e.getMessage());
		}
	}

	protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException failed) throws IOException, ServletException {

		request.setAttribute("errorMessage", failed.getLocalizedMessage());
		request.getRequestDispatcher("/login?error=403").forward(request, response);
	}
}
