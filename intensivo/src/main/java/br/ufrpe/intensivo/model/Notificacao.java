package br.ufrpe.intensivo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="notificacao")
public class Notificacao {
	
	private Integer idnotificacao;
	private String titulo;
	private String descricao;
	private Date data;
	private Boolean isAtivo;
	private Boolean isAtivoNotificacao ;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id_notificacao", insertable = false, updatable = false, precision = 11)
	public Integer getIdnotificacao() {
		return idnotificacao;
	}
	public void setIdnotificacao(Integer idnotificacao) {
		this.idnotificacao = idnotificacao;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public Boolean getIsAtivo() {
		return isAtivo;
	}
	public void setIsAtivo(Boolean isAtivo) {
		this.isAtivo = isAtivo;
	}
	public Boolean getIsAtivoNotificacao() {
		return isAtivoNotificacao;
	}
	public void setIsAtivoNotificacao(Boolean isAtivoNotificacao) {
		this.isAtivoNotificacao = isAtivoNotificacao;
	}

}
