package br.ufrpe.intensivo.model;

import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="agenda")
public class Horario {
	
	private Integer idHorario;
	private Time inicio;
	private Time fim;
	private String dia;
	private Integer materia;
	private Boolean isAtivo;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id_agenda", insertable = false, updatable = false, precision = 11)
	public Integer getIdHorario() {
		return idHorario;
	}
	public void setIdHorario(Integer idHorario) {
		this.idHorario = idHorario;
	}
	
	@Column(name = "hora_inicio", insertable = true, updatable = true)
	public Time getInicio() {
		return inicio;
	}
	public void setInicio(Time inicio) {
		this.inicio = inicio;
	}
	
	@Column(name = "hora_fim", insertable = true, updatable = true)
	public Time getFim() {
		return fim;
	}
	public void setFim(Time fim) {
		this.fim = fim;
	}
	
	@Column(name = "dia_da_semana", insertable = true, updatable = true, precision = 255)
	public String getDia() {
		return dia;
	}
	public void setDia(String dia) {
		this.dia = dia;
	}
	
	@Column(name = "isAtivo", insertable = true, updatable = true, precision = 1)
	public Boolean getIsAtivo() {
		return isAtivo;
	}
	public void setIsAtivo(Boolean isAtivo) {
		this.isAtivo = isAtivo;
	}
	
	@Column(name = "materia_id_materia", insertable = true, updatable = true, precision = 11)
	public Integer getMateria() {
		return materia;
	}
	public void setMateria(Integer materia) {
		this.materia = materia;
	}

}
