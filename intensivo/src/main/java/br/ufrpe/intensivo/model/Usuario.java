package br.ufrpe.intensivo.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "usuario")
public class Usuario implements Auditavel {

	private Integer id_usuario;
	private String email;
	private String senha;
	private String nome;

	private Date dataNascimento;
	private String escolaQueEstuda;
	private String escolaridade;
	private String cpf;
	private String token;
	private String login;
	private Long usuarioIdUsuario;
	private Long tipoUsuarioIdTipoUsuario;
	private String bairro;
	private String cidade;
	private Boolean adiplenteOrInadiplente;
	private Boolean isAtivo;
	private String turma1;
	private String turma2;
	private String turma3;
	private String turma4;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id_usuario", insertable = false, updatable = false, precision = 11)
	public Integer getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(Integer idusuario) {
		this.id_usuario = idusuario;
	}

	@Column(name = "email", insertable = true, updatable = true, nullable = false, precision = 50)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "senha", insertable = true, updatable = true, nullable = false, precision = 16)
	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	@Column(name = "nome", insertable = true, updatable = true, nullable = false, precision = 100)
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Column(name = "data_nascimento", insertable = true, updatable = true)
	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	@Column(name = "escola_que_estuda", insertable = true, updatable = true)
	public String getEscolaQueEstuda() {
		return escolaQueEstuda;
	}

	public void setEscolaQueEstuda(String escolaQueEstuda) {
		this.escolaQueEstuda = escolaQueEstuda;
	}

	@Column(name = "escolaridade", insertable = true, updatable = true)
	public String getEscolaridade() {
		return escolaridade;
	}

	public void setEscolaridade(String escolaridade) {
		this.escolaridade = escolaridade;
	}

	@Column(name = "cpf", insertable = true, updatable = true, precision = 255)
	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	@Column(name = "token", insertable = true, updatable = true, precision = 255)
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Column(name = "login", insertable = true, updatable = true, precision = 255)
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Column(name = "usuario_id_usuario", insertable = true, updatable = true, precision = 11)
	public Long getUsuarioIdUsuario() {
		return usuarioIdUsuario;
	}

	public void setUsuarioIdUsuario(Long usuarioIdUsuario) {
		this.usuarioIdUsuario = usuarioIdUsuario;
	}

	@Column(name = "tipo_usuario_id_tipo_usuario", insertable = true, updatable = true, precision = 11)
	public Long getTipoUsuarioIdTipoUsuario() {
		return tipoUsuarioIdTipoUsuario;
	}

	public void setTipoUsuarioIdTipoUsuario(Long tipoUsuarioIdTipoUsuario) {
		this.tipoUsuarioIdTipoUsuario = tipoUsuarioIdTipoUsuario;
	}

	@Column(name = "bairro", insertable = true, updatable = true, precision = 255)
	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	@Column(name = "cidade", insertable = true, updatable = true, precision = 255)
	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	@Column(name = "adiplente_or_inadiplente", insertable = true, updatable = true, precision = 1)
	public Boolean getAdiplenteOrInadiplente() {
		return adiplenteOrInadiplente;
	}

	public void setAdiplenteOrInadiplente(Boolean adiplenteOrInadiplente) {
		this.adiplenteOrInadiplente = adiplenteOrInadiplente;
	}

	@Column(name = "isAtivo", insertable = true, updatable = true, precision = 1)
	public Boolean getIsAtivo() {
		return isAtivo;
	}

	public void setIsAtivo(Boolean isAtivo) {
		this.isAtivo = isAtivo;
	}

	@Column(name = "turma_1", insertable = true, updatable = true, precision = 255)
	public String getTurma1() {
		return turma1;
	}

	public void setTurma1(String turma1) {
		this.turma1 = turma1;
	}

	@Column(name = "turma_2", insertable = true, updatable = true, precision = 255)
	public String getTurma2() {
		return turma2;
	}

	public void setTurma2(String turma2) {
		this.turma2 = turma2;
	}

	@Column(name = "turma_3", insertable = true, updatable = true, precision = 255)
	public String getTurma3() {
		return turma3;
	}

	public void setTurma3(String turma3) {
		this.turma3 = turma3;
	}

	@Column(name = "turma_4", insertable = true, updatable = true, precision = 255)
	public String getTurma4() {
		return turma4;
	}

	public void setTurma4(String turma4) {
		this.turma4 = turma4;
	}

	@Override
	@Transient
	public Map<String, Object> getValorObjeto() {
		HashMap<String, Object> map = new HashMap<String, Object>();

		map.put("idavaliacaousuario", id_usuario);
		map.put("nome", nome);
		map.put("email", email);

		return map;
	}
}
