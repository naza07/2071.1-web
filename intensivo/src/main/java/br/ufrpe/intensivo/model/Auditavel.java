package br.ufrpe.intensivo.model;

import java.util.Map;

public interface Auditavel {
	Map<String, Object> getValorObjeto();
}
