package br.ufrpe.intensivo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="propaganda")
public class Propaganda {
	private Integer idPropaganda;
	private String urlImagem;
	private String nomeImagem;
	private Date dataDaPropaganda;
	private Boolean isAtivo;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id_propaganda", insertable = false, updatable = false, precision = 11)
	public Integer getIdPropaganda() {
		return idPropaganda;
	}
	public void setIdPropaganda(Integer idPropaganda) {
		this.idPropaganda = idPropaganda;
	}
	
	@Column(name = "url_imagem", insertable = true, updatable = true, precision = 300)
	public String getUrlImagem() {
		return urlImagem;
	}
	public void setUrlImagem(String urlImagem) {
		this.urlImagem = urlImagem;
	}
	
	@Column(name = "nome_imagem", insertable = true, updatable = true, precision = 255)
	public String getNomeImagem() {
		return nomeImagem;
	}
	public void setNomeImagem(String nomeImagem) {
		this.nomeImagem = nomeImagem;
	}
	
	@Column(name = "data_da_propaganda", insertable = true, updatable = true)
	public Date getDataDaPropaganda() {
		return dataDaPropaganda;
	}
	public void setDataDaPropaganda(Date dataDaPropaganda) {
		this.dataDaPropaganda = dataDaPropaganda;
	}
	
	@Column(name = "isAtivo", insertable = true, updatable = true)
	public Boolean getIsAtivo() {
		return isAtivo;
	}
	public void setIsAtivo(Boolean isAtivo) {
		this.isAtivo = isAtivo;
	}
	
	

}
