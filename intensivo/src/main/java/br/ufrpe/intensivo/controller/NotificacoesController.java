package br.ufrpe.intensivo.controller;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.base.Throwables;

import br.ufrpe.intensivo.exception.NegocioException;
import br.ufrpe.intensivo.model.Notificacao;
import br.ufrpe.intensivo.model.Usuario;
import br.ufrpe.intensivo.service.NotificacaoService;
import br.ufrpe.intensivo.service.UsuarioService;
import br.ufrpe.intensivo.util.Util;
import br.ufrpe.intensivo.vo.NotificacaoVO;
import br.ufrpe.intensivo.vo.UsuarioVO;

@Controller
@RequestMapping(value = "/protected/notificacoes")
public class NotificacoesController {

	@Autowired
	NotificacaoService notificacaoService;

	@Autowired
	private MessageSource messageSource;

	public NotificacoesController() {
	}

	public NotificacoesController(NotificacaoService notificacaoService, MessageSource messageSource) {
		super();
		this.notificacaoService = notificacaoService;
		this.messageSource = messageSource;
	}

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView notificacoes() throws NegocioException {
		return new ModelAndView("notificacao");
	}

	@RequestMapping(value = "/insertNotificacao", method = RequestMethod.POST)
	@ResponseBody
	public Long insertNotificacao(@RequestBody NotificacaoVO notificacaoVO) throws NegocioException {

		Notificacao notificacao = new Notificacao();
		notificacao.setIdnotificacao(notificacaoVO.getIdnotificacao());
		notificacao.setTitulo(notificacaoVO.getTitulo());
		notificacao.setDescricao(notificacaoVO.getDescricao());
		notificacao.setData(notificacaoVO.getData());
		notificacao.setIsAtivo(true);
		notificacao.setIsAtivoNotificacao(true);
		notificacaoService.saveNotificacao(notificacao);

		return 1L;
	}

	@RequestMapping(value = "/updateNotificacao", method = RequestMethod.POST)
	@ResponseBody
	public Long updateNotificacao(@RequestBody NotificacaoVO notificacaoVO) throws NegocioException {
		Notificacao notificacaoUpdate = notificacaoService.findById(notificacaoVO.getIdnotificacao());
		if (notificacaoUpdate == null) {
			throw new NegocioException("erro.notificacao.1", messageSource);
		}
		Notificacao notificacao = new Notificacao();
		notificacao.setIdnotificacao(notificacaoVO.getIdnotificacao());
		notificacao.setTitulo(notificacaoVO.getTitulo());
		notificacao.setDescricao(notificacaoVO.getDescricao());
		notificacao.setData(notificacaoVO.getData());
		notificacao.setIsAtivo(true);
		notificacao.setIsAtivoNotificacao(true);
		notificacaoService.saveNotificacao(notificacao);
		return 1L;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public Long deleteNotificacao(@RequestParam("idNotificacao") Integer idNotificacao) throws NegocioException {
		Notificacao notificacaoUpdate = notificacaoService.findById(idNotificacao);
		notificacaoUpdate.setIsAtivo(false);
		notificacaoService.saveNotificacao(notificacaoUpdate);
		return 1L;
	}

	@RequestMapping(value = "/getNotificacoes", method = RequestMethod.POST)
	@ResponseBody
	public List<NotificacaoVO> findAllValidNotificacoes() throws NegocioException {

		List<Notificacao> notificacoes = new ArrayList<>();
		List<NotificacaoVO> notificacoesVO = new ArrayList<>();

		notificacoes = notificacaoService.findAllValidNotificacoes();

		for (int i = 0; i < notificacoes.size(); i++) {
			NotificacaoVO notificacaoVO = new NotificacaoVO();
			notificacaoVO.setIdnotificacao(notificacoes.get(i).getIdnotificacao());
			notificacaoVO.setTitulo(notificacoes.get(i).getTitulo());
			notificacaoVO.setDescricao(notificacoes.get(i).getDescricao());
			notificacaoVO.setData(notificacoes.get(i).getData());
			notificacoesVO.add(notificacaoVO);
		}

		return notificacoesVO;
	}

	@RequestMapping("error")
	public String customError(HttpServletRequest request, HttpServletResponse response, Model model) {

		Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
		Throwable throwable = (Throwable) request.getAttribute("javax.servlet.error.exception");

		String exceptionMessage = getExceptionMessage(throwable, statusCode);

		if (exceptionMessage == null) {
			exceptionMessage = "Erro inesperado: " + Throwables.getRootCause(throwable).toString() + " "
					+ Throwables.getRootCause(throwable).getStackTrace()[0].toString();
		}

		String requestUri = (String) request.getAttribute("javax.servlet.error.request_uri");

		if (requestUri == null) {
			requestUri = "Unknown";
		}

		String message = exceptionMessage;
		model.addAttribute("errorMessage", message);

		return "customError";
	}

	private String getExceptionMessage(Throwable throwable, Integer statusCode) {
		if (throwable != null) {
			return Throwables.getRootCause(throwable).getMessage();
		}
		HttpStatus httpStatus = HttpStatus.valueOf(statusCode);
		return httpStatus.getReasonPhrase();
	}
}