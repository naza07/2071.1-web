package br.ufrpe.intensivo.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tiles.autotag.core.runtime.annotation.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.base.Throwables;

import br.ufrpe.intensivo.exception.NegocioException;
import br.ufrpe.intensivo.model.Usuario;
import br.ufrpe.intensivo.repository.UsuarioRepository;
import br.ufrpe.intensivo.service.UsuarioService;
import br.ufrpe.intensivo.vo.Cidade;
import br.ufrpe.intensivo.vo.UsuarioVO;

@Controller
@RequestMapping(value = "/protected/usuarios")
public class UsuariosController {

	@Autowired
	UsuarioService usuarioService;

	@Autowired
	private MessageSource messageSource;

	public UsuariosController() {
	}

	public UsuariosController(UsuarioService usuarioService, MessageSource messageSource) {
		super();
		this.usuarioService = usuarioService;
		this.messageSource = messageSource;
	}

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView welcome() throws NegocioException {
		return new ModelAndView("usuarios");
	}

	@RequestMapping(value = "/insertResponsavel", method = RequestMethod.POST)
	@ResponseBody
	public Long insertUsuarioResponsavel(@RequestBody UsuarioVO usuarioVO) throws NegocioException {

		Usuario usuario = new Usuario();
		usuario.setNome(usuarioVO.getNome());
		usuario.setCpf(usuarioVO.getCpf());
		usuario.setEmail(usuarioVO.getEmail());
		usuario.setLogin(usuarioVO.getEmail());
		usuario.setSenha(usuarioVO.getSenha());
		usuario.setTipoUsuarioIdTipoUsuario(3L);
		usuario.setIsAtivo(true);

		usuarioService.salvarUsuarioWeb(usuario);

		return 1L;
	}

	@RequestMapping(value = "/updateResponsavel", method = RequestMethod.POST)
	@ResponseBody
	public Long updateUsuarioResponsavel(@RequestBody UsuarioVO usuarioVO) throws NegocioException {

		Usuario usuario = new Usuario();
		usuario.setId_usuario(usuarioVO.getId_usuario());
		usuario.setNome(usuarioVO.getNome());
		usuario.setCpf(usuarioVO.getCpf());
		usuario.setEmail(usuarioVO.getEmail());
		usuario.setLogin(usuarioVO.getEmail());
		usuario.setSenha(usuarioVO.getSenha());
		usuario.setTipoUsuarioIdTipoUsuario(3L);
		usuario.setIsAtivo(true);

		usuarioService.alterarUsuario(usuario);

		return 1L;
	}

	@RequestMapping(value = "/insertProfessor", method = RequestMethod.POST)
	@ResponseBody
	public Long insertUsuarioProfessor(@RequestBody UsuarioVO usuarioVO) throws NegocioException {

		Usuario usuario = new Usuario();
		usuario.setNome(usuarioVO.getNome());
		usuario.setCpf(usuarioVO.getCpf());
		usuario.setEmail(usuarioVO.getEmail());
		usuario.setLogin(usuarioVO.getEmail());
		usuario.setSenha(usuarioVO.getSenha());
		usuario.setTipoUsuarioIdTipoUsuario(2L);
		usuario.setIsAtivo(true);

		usuarioService.salvarUsuarioWeb(usuario);

		return 1L;
	}

	@RequestMapping(value = "/updateProfessor", method = RequestMethod.POST)
	@ResponseBody
	public Long updateUsuarioProfessor(@RequestBody UsuarioVO usuarioVO) throws NegocioException {

		Usuario usuario = new Usuario();
		usuario.setId_usuario(usuarioVO.getId_usuario());
		usuario.setNome(usuarioVO.getNome());
		usuario.setCpf(usuarioVO.getCpf());
		usuario.setEmail(usuarioVO.getEmail());
		usuario.setLogin(usuarioVO.getEmail());
		usuario.setSenha(usuarioVO.getSenha());
		usuario.setTipoUsuarioIdTipoUsuario(2L);
		usuario.setIsAtivo(true);

		usuarioService.alterarUsuario(usuario);

		return 1L;
	}

	@RequestMapping(value = "/insertAluno", method = RequestMethod.POST)
	@ResponseBody
	public Long insertUsuarioAluno(@RequestBody UsuarioVO usuarioVO) throws NegocioException {

		Usuario usuario = new Usuario();
		usuario.setNome(usuarioVO.getNome());
		usuario.setCpf(usuarioVO.getCpf());
		usuario.setEmail(usuarioVO.getEmail());
		usuario.setLogin(usuarioVO.getEmail());
		usuario.setSenha(usuarioVO.getSenha());
		usuario.setTipoUsuarioIdTipoUsuario(1L);
		usuario.setIsAtivo(true);
		usuario.setDataNascimento(usuarioVO.getDataNascimento());
		usuario.setEscolaridade(usuarioVO.getEscolaridade());
		usuario.setEscolaQueEstuda(usuarioVO.getEscolaQueEstuda());
		usuario.setCidade(usuarioVO.getCidade());
		usuario.setBairro(usuarioVO.getBairro());

		usuarioService.salvarUsuarioWeb(usuario);

		return 1L;
	}

	@RequestMapping(value = "/updateAluno", method = RequestMethod.POST)
	@ResponseBody
	public Long updateUsuarioAluno(@RequestBody UsuarioVO usuarioVO) throws NegocioException {

		Usuario usuario = new Usuario();
		usuario.setId_usuario(usuarioVO.getId_usuario());
		usuario.setNome(usuarioVO.getNome());
		usuario.setCpf(usuarioVO.getCpf());
		usuario.setEmail(usuarioVO.getEmail());
		usuario.setLogin(usuarioVO.getEmail());
		usuario.setSenha(usuarioVO.getSenha());
		usuario.setTipoUsuarioIdTipoUsuario(1L);
		usuario.setIsAtivo(true);
		usuario.setDataNascimento(usuarioVO.getDataNascimento());
		usuario.setEscolaridade(usuarioVO.getEscolaridade());
		usuario.setEscolaQueEstuda(usuarioVO.getEscolaQueEstuda());
		usuario.setCidade(usuarioVO.getCidade());
		usuario.setBairro(usuarioVO.getBairro());

		usuarioService.alterarUsuario(usuario);

		return 1L;

	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public Long deleteUsuario(@RequestParam("idUsuario") Integer idUsuario) throws NegocioException {

		usuarioService.deletarUsuario(idUsuario);
		return 1L;

	}

	@RequestMapping(value = "/getUsuarios", method = RequestMethod.POST)
	@ResponseBody
	public List<UsuarioVO> pegarTodosUsuarios() throws NegocioException {
		
	
		List<Usuario> usuarios = new ArrayList<>();
		List<UsuarioVO> usuariosVO = new ArrayList<>();

		usuarios = usuarioService.consultarListaUsuarios();

		for (int i = 0; i < usuarios.size(); i++) {
			UsuarioVO usuarioVO = new UsuarioVO();
			usuarioVO.setAdiplenteOrInadiplente(usuarios.get(i).getAdiplenteOrInadiplente());
			usuarioVO.setBairro(usuarios.get(i).getBairro());
			usuarioVO.setCidade(usuarios.get(i).getCidade());
			usuarioVO.setCpf(usuarios.get(i).getCpf());
			usuarioVO.setDataNascimento(usuarios.get(i).getDataNascimento());
			usuarioVO.setEmail(usuarios.get(i).getEmail());
			usuarioVO.setEscolaQueEstuda(usuarios.get(i).getEscolaQueEstuda());
			usuarioVO.setEscolaridade(usuarios.get(i).getEscolaridade());
			usuarioVO.setNome(usuarios.get(i).getNome());
			usuarioVO.setTipoUsuarioIdTipoUsuario(usuarios.get(i).getTipoUsuarioIdTipoUsuario());
			usuarioVO.setTurma1(usuarios.get(i).getTurma1());
			usuarioVO.setTurma2(usuarios.get(i).getTurma2());
			usuarioVO.setTurma3(usuarios.get(i).getTurma3());
			usuarioVO.setTurma4(usuarios.get(i).getTurma4());
			usuarioVO.setId_usuario(usuarios.get(i).getId_usuario());
			usuariosVO.add(usuarioVO);
		}

		return usuariosVO;
	}

	@RequestMapping("error")
	public String customError(HttpServletRequest request, HttpServletResponse response, Model model) {

		Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
		Throwable throwable = (Throwable) request.getAttribute("javax.servlet.error.exception");

		String exceptionMessage = getExceptionMessage(throwable, statusCode);

		if (exceptionMessage == null) {
			exceptionMessage = "Erro inesperado: " + Throwables.getRootCause(throwable).toString() + " "
					+ Throwables.getRootCause(throwable).getStackTrace()[0].toString();
		}

		String requestUri = (String) request.getAttribute("javax.servlet.error.request_uri");

		if (requestUri == null) {
			requestUri = "Unknown";
		}

		String message = exceptionMessage;
		model.addAttribute("errorMessage", message);

		return "customError";
	}

	private String getExceptionMessage(Throwable throwable, Integer statusCode) {
		if (throwable != null) {
			return Throwables.getRootCause(throwable).getMessage();
		}
		HttpStatus httpStatus = HttpStatus.valueOf(statusCode);
		return httpStatus.getReasonPhrase();
	}
}