package br.ufrpe.intensivo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import br.ufrpe.intensivo.exception.NegocioException;
import br.ufrpe.intensivo.model.Usuario;
import br.ufrpe.intensivo.vo.Cidade;
import br.ufrpe.intensivo.vo.Endereco;
import br.ufrpe.intensivo.vo.UsuarioVO;

@Controller
@RequestMapping("/painel") 
public class PainelController {
	
	@Autowired
	private MessageSource messageSource;
	
	@RequestMapping()
	public ModelAndView doGet () {
		return new ModelAndView ("painel");
	}
	
	@RequestMapping(value = "/getAll", method = RequestMethod.POST)
	@ResponseBody
	public Long insertUsuarioResponsavel(@RequestBody UsuarioVO usuarioVO) throws NegocioException {
		// ainda n funcionando
		try {
			//Busca de localidades são lourenço da mata
			String urlCodigoCidade = "http://servicos.cptec.inpe.br/XML/listaCidades?city=sao%20lourenco%20da%20mata";
			RestTemplate restTemplate = new RestTemplate();
			Cidade cod = restTemplate.getForObject(urlCodigoCidade, Cidade.class, 200);
			//busca da previsão 4971
			String urlPrevisao = "http://servicos.cptec.inpe.br/XML/cidade/"+cod+"/previsao.xml";
			
		} catch (Exception e) {
			throw new NegocioException("erro.cadastro.4", messageSource);
		}

		return 1L;
	}
	
	
}
