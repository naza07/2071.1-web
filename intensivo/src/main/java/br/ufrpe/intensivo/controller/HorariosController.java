package br.ufrpe.intensivo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.base.Throwables;

import br.ufrpe.intensivo.exception.NegocioException;
import br.ufrpe.intensivo.model.Horario;
import br.ufrpe.intensivo.service.HorariosService;
import br.ufrpe.intensivo.vo.HorarioVO;

@Controller
@RequestMapping(value = "/protected/horarios")
public class HorariosController {
	
	@Autowired
	HorariosService horariosService;

	@Autowired
	private MessageSource messageSource;
	
	public HorariosController() {

	}
	
	public HorariosController(HorariosService horariosService, MessageSource messageSource) {
		super();
		this.horariosService = horariosService;
		this.messageSource = messageSource;
	}
	
	@RequestMapping(method=RequestMethod.GET)
    public ModelAndView welcome() throws NegocioException {
        return new ModelAndView("horarios");
    }
	
	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	@ResponseBody
	public Long insertHorario(@RequestBody HorarioVO horarioVO) throws NegocioException {

		Horario horario = new Horario();
		horario.setIdHorario(horarioVO.getIdHorario());
		horario.setDia(horarioVO.getDia());
		horario.setFim(horarioVO.getFim());
		horario.setInicio(horarioVO.getInicio());
		horario.setMateria(horarioVO.getMateria());
		horario.setIsAtivo(true);
		
		horariosService.saveHorario(horario);
		
		return 1L;
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public Long updateHorario(@RequestBody HorarioVO horarioVO) throws NegocioException {
		Horario horarioUpdate = horariosService.findById(horarioVO.getIdHorario());
		if (horarioUpdate == null) {
			throw new NegocioException("erro.horario.1", messageSource);
		}

		Horario horario = new Horario();
		horario.setIdHorario(horarioVO.getIdHorario());
		horario.setDia(horarioVO.getDia());
		horario.setFim(horarioVO.getFim());
		horario.setInicio(horarioVO.getInicio());
		horario.setMateria(horarioVO.getMateria());
		horario.setIsAtivo(true);
		
		horariosService.saveHorario(horario);
		
		return 1L;
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public Long deleteHorario(@RequestParam("idHorario") Integer idHorario) throws NegocioException {
		Horario horario = horariosService.findById(idHorario);
		horario.setIsAtivo(false);
		horariosService.saveHorario(horario);
		return 1L;
	}
	
	@RequestMapping(value = "/getAllHorario", method = RequestMethod.POST)
	@ResponseBody
	public List<HorarioVO> findAllValidHorario() throws NegocioException {

		List<Horario> horarios = new ArrayList<>();
		List<HorarioVO> horariosVO = new ArrayList<>();

		horarios = horariosService.findAllValidHorario();

		for (int i = 0; i < horarios.size(); i++) {
			HorarioVO horarioVO = new HorarioVO();
			horarioVO.setIdHorario(horarios.get(i).getIdHorario());
			horarioVO.setDia(horarios.get(i).getDia());
			horarioVO.setFim(horarios.get(i).getFim());
			horarioVO.setInicio(horarios.get(i).getInicio());
			horarioVO.setIsAtivo(horarios.get(i).getIsAtivo());
			horarioVO.setMateria(horarios.get(i).getMateria());
		

			horariosVO.add(horarioVO);
		}

		return horariosVO;
	}
	

	@RequestMapping("error")
	public String customError(HttpServletRequest request,
			HttpServletResponse response, Model model) {

		Integer statusCode = (Integer) request
				.getAttribute("javax.servlet.error.status_code");
		Throwable throwable = (Throwable) request
				.getAttribute("javax.servlet.error.exception");

		String exceptionMessage = getExceptionMessage(throwable, statusCode);

		if (exceptionMessage == null){
			exceptionMessage = "Erro inesperado: "+Throwables.getRootCause(throwable).toString()+" "+Throwables.getRootCause(throwable).getStackTrace()[0].toString();
		}

		String requestUri = (String) request.getAttribute("javax.servlet.error.request_uri");

		if (requestUri == null) {
			requestUri = "Unknown";
		}

		String message = exceptionMessage;
		model.addAttribute("errorMessage", message);

		return "customError";
	}

	private String getExceptionMessage(Throwable throwable, Integer statusCode) {
		if (throwable != null) {
			return Throwables.getRootCause(throwable).getMessage();
		}
		HttpStatus httpStatus = HttpStatus.valueOf(statusCode);
		return httpStatus.getReasonPhrase();
	}    
}