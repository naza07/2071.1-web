package br.ufrpe.intensivo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.base.Throwables;

import br.ufrpe.intensivo.exception.NegocioException;
import br.ufrpe.intensivo.model.Material;
import br.ufrpe.intensivo.service.MaterialService;
import br.ufrpe.intensivo.vo.MaterialVO;

@Controller
@RequestMapping(value = "/protected/material")
public class MaterialController {

	@Autowired
	MaterialService materialService;

	@Autowired
	private MessageSource messageSource;
	
	public MaterialController() {

	}
	
	public MaterialController(MaterialService materialService, MessageSource messageSource) {
		super();
		this.materialService = materialService;
		this.messageSource = messageSource;
	}
	
	@RequestMapping(method=RequestMethod.GET)
    public ModelAndView welcome() throws NegocioException {
        return new ModelAndView("material");
    }
	
	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	@ResponseBody
	public Long insertMaterial(@RequestBody MaterialVO materialVO) throws NegocioException {

		Material material = new Material();
		material.setIdMaterial(materialVO.getIdMaterial());
		material.setTitulo(materialVO.getTitulo());
		material.setDescricao(materialVO.getDescricao());
		material.setLink(materialVO.getLink());
		material.setData(materialVO.getData());
		material.setIsAtivo(true);
		material.setTipoUsuario(3);
		
		materialService.saveMaterial(material);
		
		return 1L;
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public Long updateNotificacao(@RequestBody MaterialVO materialVO) throws NegocioException {
		Material materialUpdate = materialService.findById(materialVO.getIdMaterial());
		if (materialUpdate == null) {
			throw new NegocioException("erro.material.1", messageSource);
		}

		Material material = new Material();
		material.setIdMaterial(materialVO.getIdMaterial());
		material.setTitulo(materialVO.getTitulo());
		material.setDescricao(materialVO.getDescricao());
		material.setLink(materialVO.getLink());
		material.setData(materialVO.getData());
		material.setIsAtivo(true);
		material.setTipoUsuario(3);

		
		materialService.saveMaterial(material);
		
		return 1L;
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public Long deleteNotificacao(@RequestParam("idMaterial") Integer idMaterial) throws NegocioException {
		Material material = materialService.findById(idMaterial);
		material.setIsAtivo(false);
		materialService.saveMaterial(material);
		return 1L;
	}
	
	@RequestMapping(value = "/getAllMaterial", method = RequestMethod.POST)
	@ResponseBody
	public List<MaterialVO> findAllValidMaterial() throws NegocioException {

		List<Material> materiais = new ArrayList<>();
		List<MaterialVO> materiaisVO = new ArrayList<>();

		materiais = materialService.findAllValidMaterial();

		for (int i = 0; i < materiais.size(); i++) {
			MaterialVO materialVO = new MaterialVO();
			materialVO.setIdMaterial(materiais.get(i).getIdMaterial());
			materialVO.setTitulo(materiais.get(i).getTitulo());
			materialVO.setDescricao(materiais.get(i).getDescricao());
			materialVO.setLink(materiais.get(i).getLink());
			materialVO.setData(materiais.get(i).getData());
			materialVO.setIsAtivo(materiais.get(i).getIsAtivo());

			materiaisVO.add(materialVO);
		}

		return materiaisVO;
	}

	@RequestMapping("error")
	public String customError(HttpServletRequest request,
			HttpServletResponse response, Model model) {

		Integer statusCode = (Integer) request
				.getAttribute("javax.servlet.error.status_code");
		Throwable throwable = (Throwable) request
				.getAttribute("javax.servlet.error.exception");

		String exceptionMessage = getExceptionMessage(throwable, statusCode);

		if (exceptionMessage == null){
			exceptionMessage = "Erro inesperado: "+Throwables.getRootCause(throwable).toString()+" "+Throwables.getRootCause(throwable).getStackTrace()[0].toString();
		}

		String requestUri = (String) request.getAttribute("javax.servlet.error.request_uri");

		if (requestUri == null) {
			requestUri = "Unknown";
		}

		String message = exceptionMessage;
		model.addAttribute("errorMessage", message);

		return "customError";
	}

	private String getExceptionMessage(Throwable throwable, Integer statusCode) {
		if (throwable != null) {
			return Throwables.getRootCause(throwable).getMessage();
		}
		HttpStatus httpStatus = HttpStatus.valueOf(statusCode);
		return httpStatus.getReasonPhrase();
	}    
}