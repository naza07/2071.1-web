package br.ufrpe.intensivo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/login")
public class LoginController {

	@RequestMapping()
	public ModelAndView doGet () {
		return new ModelAndView ("login");
	}
}
