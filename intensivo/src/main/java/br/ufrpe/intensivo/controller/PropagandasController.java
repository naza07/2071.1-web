package br.ufrpe.intensivo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.base.Throwables;

import br.ufrpe.intensivo.exception.NegocioException;
import br.ufrpe.intensivo.model.Notificacao;
import br.ufrpe.intensivo.model.Propaganda;
import br.ufrpe.intensivo.service.NotificacaoService;
import br.ufrpe.intensivo.service.PropagandaService;
import br.ufrpe.intensivo.vo.NotificacaoVO;
import br.ufrpe.intensivo.vo.PropagandaVO;

@Controller
@RequestMapping(value = "/protected/propagandas")
public class PropagandasController {
	
	@Autowired
	PropagandaService propagandaService;

	@Autowired
	private MessageSource messageSource;

	public PropagandasController() {
	}

	public PropagandasController(PropagandaService propagandaService, MessageSource messageSource) {
		this.propagandaService = propagandaService;
		this.messageSource = messageSource;
	}

	
	@RequestMapping(method = RequestMethod.GET)
    public ModelAndView welcome() throws NegocioException {
        return new ModelAndView("propagandas");
    }
	

	@RequestMapping(value = "/insertPropaganda", method = RequestMethod.POST)
	@ResponseBody
	public Long insertPropaganda(@RequestBody PropagandaVO propagandaVO) throws NegocioException {	
		/*Propaganda p = propagandaService.findById(propagandaVO.getId_propaganda());
		
		if (p != null) {
			throw new NegocioException("erro.cadastro.1", messageSource);
		}*/
		
		Propaganda propaganda = new Propaganda();
		propaganda.setNomeImagem(propagandaVO.getNome());
		propaganda.setDataDaPropaganda(propagandaVO.getData());
		propaganda.setUrlImagem(propagandaVO.getImagem_url());
		propaganda.setIsAtivo(true);
		propagandaService.savePropaganda(propaganda);
		return 1L;
	}

	@RequestMapping(value = "/updatePropaganda", method = RequestMethod.POST)
	@ResponseBody
	public Long updatePropaganda(@RequestBody PropagandaVO propagandaVO) throws NegocioException {
		Propaganda propagandaUpdate = propagandaService.findById(propagandaVO.getId_propaganda());
		if (propagandaUpdate == null) {
			throw new NegocioException("erro.notificacao.1", messageSource);
		}
		Propaganda propaganda = new Propaganda();
		propaganda.setIdPropaganda(propagandaVO.getId_propaganda());
		propaganda.setNomeImagem(propagandaVO.getNome());
		propaganda.setDataDaPropaganda(propagandaVO.getData());
		propaganda.setUrlImagem(propagandaVO.getImagem_url());
		propaganda.setIsAtivo(true);
		propagandaService.savePropaganda(propaganda);
		return 1L;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public Long deletePropaganda(@RequestParam("idPropaganda") Integer idPropaganda) throws NegocioException {
		Propaganda propagandaUpdate = propagandaService.findById(idPropaganda);
		propagandaUpdate.setIsAtivo(false);
		propagandaService.savePropaganda(propagandaUpdate);
		return 1L;
	}

	@RequestMapping(value = "/getPropagandas", method = RequestMethod.POST)
	@ResponseBody
	public List<PropagandaVO> findAllValidPropagandas() throws NegocioException {

		List<Propaganda> propagandas = new ArrayList<>();
		List<PropagandaVO> propagandasVO = new ArrayList<>();

		propagandas = propagandaService.findAllValidPropagandas();

		for (int i = 0; i < propagandas.size(); i++) {
			PropagandaVO propagandaVO = new PropagandaVO();
			propagandaVO.setId_propaganda(propagandas.get(i).getIdPropaganda());
			propagandaVO.setNome(propagandas.get(i).getNomeImagem());
			propagandaVO.setData(propagandas.get(i).getDataDaPropaganda());
			propagandaVO.setImagem_url(propagandas.get(i).getUrlImagem());
			propagandasVO.add(propagandaVO);
		}

		return propagandasVO;
	}

	@RequestMapping("error")
	public String customError(HttpServletRequest request,
			HttpServletResponse response, Model model) {

		Integer statusCode = (Integer) request
				.getAttribute("javax.servlet.error.status_code");
		Throwable throwable = (Throwable) request
				.getAttribute("javax.servlet.error.exception");

		String exceptionMessage = getExceptionMessage(throwable, statusCode);

		if (exceptionMessage == null){
			exceptionMessage = "Erro inesperado: "+Throwables.getRootCause(throwable).toString()+" "+Throwables.getRootCause(throwable).getStackTrace()[0].toString();
		}

		String requestUri = (String) request.getAttribute("javax.servlet.error.request_uri");

		if (requestUri == null) {
			requestUri = "Unknown";
		}

		String message = exceptionMessage;
		model.addAttribute("errorMessage", message);

		return "customError";
	}

	private String getExceptionMessage(Throwable throwable, Integer statusCode) {
		if (throwable != null) {
			return Throwables.getRootCause(throwable).getMessage();
		}
		HttpStatus httpStatus = HttpStatus.valueOf(statusCode);
		return httpStatus.getReasonPhrase();
	}    
}