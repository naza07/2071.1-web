package br.ufrpe.intensivo.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.base.Throwables;

import br.ufrpe.intensivo.exception.NegocioException;

@Controller
public class IndexController {

	@RequestMapping(value="/", method=RequestMethod.GET)
	public ModelAndView direcionarDefault(){
		return new ModelAndView("redirect:/protected/home");
	}
	
	@RequestMapping(value = "/protected/home", method=RequestMethod.GET)
    public ModelAndView welcome() throws NegocioException {
        return new ModelAndView("home");
    }

	@RequestMapping("error")
	public String customError(HttpServletRequest request,
			HttpServletResponse response, Model model) {

		Integer statusCode = (Integer) request
				.getAttribute("javax.servlet.error.status_code");
		Throwable throwable = (Throwable) request
				.getAttribute("javax.servlet.error.exception");

		String exceptionMessage = getExceptionMessage(throwable, statusCode);

		if (exceptionMessage == null){
			exceptionMessage = "Erro inesperado: "+Throwables.getRootCause(throwable).toString()+" "+Throwables.getRootCause(throwable).getStackTrace()[0].toString();
		}

		String requestUri = (String) request.getAttribute("javax.servlet.error.request_uri");

		if (requestUri == null) {
			requestUri = "Unknown";
		}

		String message = exceptionMessage;
		model.addAttribute("errorMessage", message);

		return "customError";
	}

	private String getExceptionMessage(Throwable throwable, Integer statusCode) {
		if (throwable != null) {
			return Throwables.getRootCause(throwable).getMessage();
		}
		HttpStatus httpStatus = HttpStatus.valueOf(statusCode);
		return httpStatus.getReasonPhrase();
	}    
}