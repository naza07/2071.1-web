package br.ufrpe.intensivo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import br.ufrpe.intensivo.model.Notificacao;

public interface NotificacaoRepository extends JpaRepository<Notificacao, Integer>{
	
	
	
	
	@Query("SELECT a FROM Notificacao a WHERE a.idnotificacao = :idNotificacao and a.isAtivo = 1")
	Notificacao findByIdNotificacao(@Param("idNotificacao") Integer idNotificacao);

	@Query("SELECT a FROM Notificacao a WHERE a.isAtivo = 1")
	List<Notificacao> findAllValidNotificacoes();

}
