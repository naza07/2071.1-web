package br.ufrpe.intensivo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.ufrpe.intensivo.model.Horario;

public interface HorariosRepository extends JpaRepository<Horario, Integer>{
	
	@Query("SELECT a FROM Horario a WHERE a.idHorario = :idHorario and a.isAtivo = 1")
	Horario findByIdHorario(@Param("idHorario") Integer idHorario);

	@Query("SELECT a FROM Horario a WHERE a.isAtivo = 1")
	List<Horario> findAllValidHorario();
}
