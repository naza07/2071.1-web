package br.ufrpe.intensivo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.ufrpe.intensivo.model.Propaganda;

public interface PropagandaRepository extends JpaRepository<Propaganda, Integer> {

	@Query("SELECT a FROM Propaganda a WHERE a.idPropaganda = :idPropaganda and a.isAtivo = 1")
	Propaganda findByIdNotificacao(@Param("idPropaganda") Integer idPropaganda);

	@Query("SELECT a FROM Propaganda a WHERE a.isAtivo = 1")
	List<Propaganda> findAllValidPropagandas();

}
