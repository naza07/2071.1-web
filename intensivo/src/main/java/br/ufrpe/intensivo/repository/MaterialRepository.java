package br.ufrpe.intensivo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.ufrpe.intensivo.model.Material;

public interface MaterialRepository extends JpaRepository<Material, Integer>{
	
	@Query("SELECT a FROM Material a WHERE a.idMaterial = :idMaterial and a.isAtivo = 1")
	Material findByIdMaterial(@Param("idMaterial") Integer idMaterial);

	@Query("SELECT a FROM Material a WHERE a.isAtivo = 1")
	List<Material> findAllValidMaterial();

}
