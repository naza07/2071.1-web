package br.ufrpe.intensivo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.ufrpe.intensivo.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

	@Query("SELECT a FROM Usuario a WHERE a.email = :email and a.isAtivo = 1")
	Usuario findByEmail(@Param("email") String email);

	@Query("SELECT a FROM Usuario a WHERE a.isAtivo = 1")
	List<Usuario> findAllUsuarios();

	@Query("SELECT a FROM Usuario a WHERE a.login = :login and a.isAtivo = 1")
	Usuario findByLogin(@Param("login") String login);
}
